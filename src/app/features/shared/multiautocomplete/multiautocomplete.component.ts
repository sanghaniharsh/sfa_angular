import { FormControl } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
@Component({
  selector: 'app-multiautocomplete',
  templateUrl: './multiautocomplete.component.html',
  styleUrls: ['./multiautocomplete.component.scss']
})
export class MultiautocompleteComponent implements OnInit {
  @Output() public selectionchanged: EventEmitter<any> = new EventEmitter<any>();
  @Input() control;
  @Input() id;
  @Input() name;
  @Input() param1;
  @Input() param2;
  @Input() text;
  @Input() objectName;
  @Input() options: any[];
  @Input() isSingle;

  data = [];
  selectedItems = [];
  dropdownSettings;
  constructor() { }

  ngOnInit(): void {
    this.dropdownSettings = {
      singleSelection: this.isSingle == 'true' ? true : false,
      text: "Select Options",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.options.currentValue != changes.options.previousValue) {
      this.options = this.options.map(x => {
        let value = '';
        if (this.objectName == 'root') {
          value = x[this.param1];
        } else if (this.objectName) {
          value = x[this.objectName][this.param1] + ' ' + x[this.objectName][this.param2];
        } else {
          value = x[this.text];
        }
        return {
          id: x[this.id], itemName: value
        }
      })
    }
  }

  onItemSelect(item: any) {
    //console.log(item);
    //console.log(this.selectedItems);
    this.selectionchanged.emit();
  }
  OnItemDeSelect(item: any) {
    //console.log(item);
    //console.log(this.selectedItems);
    this.selectionchanged.emit();
  }
  onSelectAll(items: any) {
    //console.log(items);
    this.selectionchanged.emit();
  }
  onDeSelectAll(items: any) {
    //console.log(items);
    this.selectionchanged.emit();
  }

}
