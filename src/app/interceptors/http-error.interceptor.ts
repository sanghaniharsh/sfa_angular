import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, finalize } from 'rxjs/operators';
import { CommonToasterService } from '../services/common-toaster.service';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
    providedIn: 'root',
})
export class HttpErrorInterceptor implements HttpInterceptor {
    constructor(
        private commonToasterService: CommonToasterService,
        private spinner: NgxSpinnerService,
        private router: Router,
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        this.spinner.show();
        return next.handle(request)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    let errorMessage = '';
                    if (error.error instanceof ErrorEvent) {
                        // client-side error
                        errorMessage = `Error: ${error.message}`;
                        this.commonToasterService.showError("Error", errorMessage);
                    } else {
                        // server-side error
                        console.log(error);
                        if (error?.error?.error == "Unauthenticated.") {
                            localStorage.setItem('isLoggedIn', 'false');
                            localStorage.removeItem('token');
                            this.router.navigate(['/auth/login']);
                        }
                        const isArray = Array.isArray(error.error.errors);
                        const objectKeys = Object.keys(error.error.errors);
                        const isObject = objectKeys.length;
                        if (isArray) {
                            const errorsLen = Object.keys(error.error.errors).length;
                            if (errorsLen) {
                                Object.keys(error.error.errors).forEach((item, i) => {
                                    if (i < 2) {
                                        this.commonToasterService.showError(`Error in : ${item}`, `${error.error.errors[item][0]}`);
                                    }
                                });
                            }
                        }
                        else if (isObject) {
                            Object.keys(error.error.errors).forEach((item, i) => {
                                if (i < 2) {
                                    this.commonToasterService.showWarning(`${item}`, `${error.error.errors[item][0]}`);
                                }
                            });
                        }
                        else {
                            if (error.error.errors) {
                                const errors = error.error.errors.toString().slice(0, 70);
                                this.commonToasterService.showError(`Error Code: ${error.status}`, errors);
                            }
                            else {
                                this.commonToasterService.showError(`Error Code: ${error.status}`, `${error.error.message}`);
                            }
                        }
                    }
                    return throwError(error);
                }),
                finalize(() => {
                    this.spinner.hide();
                })
            )
    }
}
