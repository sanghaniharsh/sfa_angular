import { CommonToasterService } from './../../../../../services/common-toaster.service';
import { ValidatorService } from './../../../../../services/validator.service';

import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { map, startWith } from 'rxjs/operators';
import { Subscription } from 'rxjs';

import { Customer } from 'src/app/components/main/master/customer/customer-dt/customer-dt.component';
import {
  OrderModel,
  apiOrderMapper,
} from 'src/app/components/main/transaction/orders/order-models';
import { InfoModal } from '../collection-models';
import { DataEditor } from 'src/app/services/data-editor.service';
import { INVOICE_LIST } from 'src/app/features/mocks';
import { Utils } from 'src/app/services/utils';
import { CodeDialogComponent } from 'src/app/components/dialogs/code-dialog/code-dialog.component';
import { CollectionService } from '../collection.service';

@Component({
  selector: 'app-collection-form',
  templateUrl: './collection-form.component.html',
  styleUrls: ['./collection-form.component.scss'],
})
export class CollectionFormComponent implements OnInit, OnDestroy {
  public pageTitle: string;
  public enableManual = false;
  public tableHeaders: InfoModal[] = [];

  public invoices: OrderModel[] = [];
  public customers: Customer[] = [];
  public filteredCustomers: Customer[] = [];
  public payModes: InfoModal[] = [
    { id: 1, name: 'Check' },
    { id: 2, name: 'Cash' },
    { id: 3, name: 'Advance Payment' },
  ];

  public collectionForm: FormGroup;
  public collectionTypeFormControl: FormControl;
  public customerFormControl: FormControl;
  public paymentTypeFormControl: FormControl;
  public numberFormControl: FormControl;
  public dateFormControl: FormControl;
  public invoiceAmountFormControl: FormControl;
  public chequeNumberFormControl: FormControl;
  public bankNameFormControl: FormControl;
  public chequeDateFormControl: FormControl;
  public invoicesFormGroups: FormArray;

  private router: Router;
  private collectionService: CollectionService;
  private dataService: DataEditor;
  private subscriptions: Subscription[] = [];
  private route: ActivatedRoute;
  private formBuilder: FormBuilder;
  private dialogRef: MatDialog;
  private elemRef: ElementRef;
  private toaster: CommonToasterService;
  nextCommingNumberPrefix: any;

  constructor(
    collectionService: CollectionService,
    dataService: DataEditor,
    dialogRef: MatDialog,
    elemRef: ElementRef,
    toaster: CommonToasterService,
    formBuilder: FormBuilder,
    router: Router,
    route: ActivatedRoute
  ) {
    Object.assign(this, {
      collectionService,
      dataService,
      dialogRef,
      elemRef,
      formBuilder,
      router,
      route,
      toaster,
    });
  }

  public ngOnInit(): void {
    this.pageTitle = 'New Collection';
    this.tableHeaders = [...INVOICE_TABLE_HEADERS];
    this.getrouteitemgroupCode();
    this.invoices = INVOICE_LIST.data.map((item) => apiOrderMapper(item));

    this.customers = this.route.snapshot.data['resolved'].customers.data;

    this.collectionTypeFormControl = this.formBuilder.control(
      2,
      Validators.required
    );
    this.customerFormControl = this.formBuilder.control(
      '',
      Validators.required
    );
    this.paymentTypeFormControl = this.formBuilder.control(
      1,
      Validators.required
    );
    this.numberFormControl = this.formBuilder.control('', Validators.required);
    this.dateFormControl = this.formBuilder.control('', Validators.required);
    this.invoiceAmountFormControl = this.formBuilder.control('', [
      Validators.required,
      ValidatorService.numbersOnly,
    ]);
    this.chequeNumberFormControl = this.formBuilder.control('');
    this.bankNameFormControl = this.formBuilder.control('');
    this.chequeDateFormControl = this.formBuilder.control('');

    this.invoicesFormGroups = this.formBuilder.array([]);

    this.collectionForm = this.formBuilder.group({
      collection_type: this.collectionTypeFormControl,
      collection_number: this.numberFormControl,
      collection_date: this.dateFormControl,
      invoice_amount: this.invoiceAmountFormControl,
      cheque_number: this.chequeNumberFormControl,
      bank_info: this.bankNameFormControl,
      cheque_date: this.chequeDateFormControl,
      paid_invoices: this.invoicesFormGroups,
    });

    this.subscriptions.push(
      this.customerFormControl.valueChanges
        .pipe(
          startWith<string | Customer>(''),
          map((value) =>
            typeof value === 'string'
              ? value
              : `${value.user.firstname} ${value.user.lastname}`
          ),
          map((value: string) => {
            return value.length
              ? this.filterCustomers(value)
              : this.customers.slice();
          })
        )
        .subscribe((value) => {
          this.filteredCustomers = value;
        })
    );

    this.subscriptions.push(
      this.paymentTypeFormControl.valueChanges.subscribe((value) => {
        this.enableManual = value === 'manual';
        this.resetInvoicesPay();
        this.invoiceAmountFormControl.reset();
      })
    );

    this.subscriptions.push(
      this.invoiceAmountFormControl.valueChanges.subscribe((value) => {
        if (Number(value) <= 0) {
          this.resetInvoicesPay();
        } else {
          switch (this.paymentTypeFormControl.value) {
            case 1:
              this.processFifoPay();
              break;
            case 2:
              this.processLifoPay();
              break;
            case 3:
              this.processManualPay();
              break;
            default:
              this.processFifoPay();
          }
        }
      })
    );

    this.collectionTypeFormControl.valueChanges.subscribe((value) => {
      this.collectionForm.patchValue({
        cheque_number: null,
        bank_info: null,
        cheque_date: null,
      });

      if (value === 1) {
        this.collectionForm
          .get('cheque_number')
          .setValidators([Validators.required, ValidatorService.numbersOnly]);
        this.collectionForm.get('bank_info').setValidators(Validators.required);
        this.collectionForm
          .get('cheque_date')
          .setValidators(Validators.required);
      } else {
        this.collectionForm.get('cheque_number').setValidators([]);
        this.collectionForm.get('bank_info').setValidators([]);
        this.collectionForm.get('cheque_date').setValidators([]);
      }
    });
  }

  public ngOnDestroy() {
    Utils.unsubscribeAll(this.subscriptions);
  }

  public openNumberSettings(): void {
    this.dialogRef
      .open(CodeDialogComponent, {
        width: '500px',
        data: {
          title: 'Collection Code',
          functionFor: 'collection',
          code: this.numberFormControl.value,
          prefix: this.nextCommingNumberPrefix,
        },
      })
      .componentInstance.sendResponse.subscribe((res: any) => {
        if (res.type == 'manual' && res.enableButton) {
          this.numberFormControl.setValue('');
          this.numberFormControl.enable();
        } else if (res.type == 'autogenerate' && !res.enableButton) {
          this.numberFormControl.setValue(
            res.data.next_coming_number_collection
          );
          this.nextCommingNumberPrefix = res.reqData.prefix_code;
          this.numberFormControl.disable();
        }
      });
  }
  getrouteitemgroupCode() {
    let nextNumber = {
      function_for: 'collection',
    };
    this.collectionService
      .getNextCommingCode(nextNumber)
      .subscribe((res: any) => {
        if (res.status) {
          const data = res.data.number_is;
          this.nextCommingNumberPrefix = res.data.prefix_is;
          if (data) {
            this.numberFormControl.setValue(data);
            this.numberFormControl.disable();
          } else if (data == null) {
            this.numberFormControl.enable();
          }
        } else {
          this.numberFormControl.enable();
        }
      });
  }

  restrictLength(e) {
    if (e.target.value.length >= 10) {
      e.preventDefault();
    }
  }
  public processFifoPay(): void {
    let amount = 0 + Number(this.invoiceAmountFormControl.value);

    this.invoicesFormGroups.controls.forEach((item) => {
      const formGroup = item as FormGroup;
      if (amount <= formGroup.controls.total_amount.value) {
        formGroup.controls.paid_amount.setValue(0 + amount);
        amount = 0;

        return;
      } else {
        const newAmount = Number(
          (amount - formGroup.controls.total_amount.value).toFixed(2)
        );
        formGroup.controls.paid_amount.setValue(
          0 + formGroup.controls.total_amount.value
        );
        amount = newAmount;

        return;
      }
    });
  }

  public processLifoPay(): void {
    let amount = 0 + Number(this.invoiceAmountFormControl.value);
    const lastIndex = this.invoicesFormGroups.controls.length - 1;

    for (let i = lastIndex; i >= 0; i--) {
      const formGroup = this.invoicesFormGroups.controls[i] as FormGroup;
      if (amount <= formGroup.controls.total_amount.value) {
        formGroup.controls.paid_amount.setValue(0 + amount);
        amount = 0;
      } else {
        const newAmount = Number(
          (amount - formGroup.controls.total_amount.value).toFixed(2)
        );
        formGroup.controls.paid_amount.setValue(
          0 + formGroup.controls.total_amount.value
        );
        amount = newAmount;
      }
    }
  }

  public processManualPay(): void { }

  public resetInvoicesPay(): void {
    this.invoicesFormGroups.controls.forEach((item) => {
      const formGroup = item as FormGroup;
      formGroup.controls.paid_amount.setValue(0);
    });
  }

  public setupInvoicesFormControl(): void {
    this.invoices.forEach((invoice: any, index: number) => {
      const newFormGroup = this.formBuilder.group({
        id: new FormControl(invoice.id),
        invoice_date: new FormControl({
          value: invoice.invoice_date,
          disabled: true,
        }),
        invoice_number: new FormControl(invoice.invoice_number),
        total_amount: new FormControl({
          value: invoice.grand_total,
          disabled: true,
        }),
        paid_amount: new FormControl(0),
      });
      this.invoicesFormGroups.push(newFormGroup);
    });
  }

  public get invoiceForms(): AbstractControl[] {
    return this.invoicesFormGroups.controls;
  }

  public clearInvoice(index: number): void {
    this.invoices.splice(index, 1);
    this.invoicesFormGroups.removeAt(index);
  }

  public customerSelected(): void {
    const customerId = this.customerFormControl.value.user_id;
    this.collectionService.getPendingInvoice(customerId).subscribe(
      (response) => {
        this.invoicesFormGroups.controls.length = 0;
        this.invoices = response.data;
        this.setupInvoicesFormControl();
      },
      (error) => {
        //console.log(error);
      }
    );
  }

  public customerControlDisplayValue(customer: Customer): string {
    return customer
      ? `${customer.user.firstname} ${customer.user.lastname}`
      : '';
  }

  public addCustomer(): void {
    this.router.navigate(['masters/customer'], {
      queryParams: { create: true },
    });
  }

  public saveCollection(): void {
    const form = JSON.parse(JSON.stringify(this.collectionForm.value));
    form['items'] = form.paid_invoices.map((item) => ({
      invoice_id: item.id,
      amount: item.paid_amount,
    }));
    delete form['paid_invoices'];
    form.collection_number = this.numberFormControl.value;
    form.customer_id = this.customerFormControl.value.user_id;
    form.invoice_amount = this.invoiceAmountFormControl.value;
    form.collection_type = this.paymentTypeFormControl.value;
    form.payemnt_type = this.collectionTypeFormControl.value;
    form.status = '1';
    form.salesman_id = '';
    form.transaction_number = '';
    form.source = 3;
    this.collectionService.addCollection(form).subscribe(
      (response) => {
        this.toaster.showSuccess(
          'Success',
          'Collection has been added successfuly.'
        );
        this.router.navigateByUrl('/transaction/collection');
      },
      (error) => { }
    );
  }

  private filterCustomers(customerName: string): Customer[] {
    const filterValue = customerName.toLowerCase();

    return this.customers.filter(
      (customer) =>
        customer.user.firstname.toLowerCase().includes(filterValue) ||
        customer.user.lastname.toLowerCase().includes(filterValue)
    );
  }
}

const INVOICE_TABLE_HEADERS: InfoModal[] = [
  { id: 0, name: '#' },
  { id: 1, name: 'Invoice Date' },
  { id: 2, name: 'Invoice Number' },
  { id: 4, name: 'Total Amount' },
  { id: 5, name: 'Paid Amount' },
];
