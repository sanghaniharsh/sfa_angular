import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CollectionModel, InfoModal } from '../collection-models';
import {
  OrderModel,
  apiOrderMapper,
} from 'src/app/components/main/transaction/orders/order-models';
import { ApiService } from 'src/app/services/api.service';
import { DataEditor } from 'src/app/services/data-editor.service';
import { CollectionService } from '../collection.service';
import { CommonToasterService } from 'src/app/services/common-toaster.service';
import { CompDataServiceType } from 'src/app/services/constants';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-collection-detail',
  templateUrl: './collection-detail.component.html',
  styleUrls: ['./collection-detail.component.scss'],
})
export class CollectionDetailComponent implements OnInit, OnDestroy {
  public uuid: string;
  @Output() public detailsClosed: EventEmitter<any> = new EventEmitter<any>();
  @Input() public collectionData: any;
  @Input() public isDetailVisible: boolean;
  @Output() public toggleHistory: EventEmitter<any> = new EventEmitter<any>();
  emailData: any;
  public tableHeaders: InfoModal[] = [];
  public invoices: OrderModel[] = [];
  private dataService: DataEditor;
  private collectionService: CollectionService;
  private subscriptions: Subscription[] = [];
  private route: ActivatedRoute;
  public hasApprovalPending: boolean = false;
  private apiService: ApiService;

  collectionTemplate: any;
  private sanitizer: DomSanitizer;

  constructor(
    collectionService: CollectionService,
    dataService: DataEditor,
    dialogRef: MatDialog,
    elemRef: ElementRef,
    formBuilder: FormBuilder,
    router: Router,
    route: ActivatedRoute,
    sanitizer: DomSanitizer,
    private commonToasterService: CommonToasterService
  ) {
    Object.assign(this, {
      collectionService,
      dataService,
      dialogRef,
      elemRef,
      formBuilder,
      router,
      route,
      sanitizer,
    });
  }

  public ngOnInit(): void {
    this.tableHeaders = [...INVOICE_TABLE_HEADERS];
  }

  onToggleHistory() {
    this.toggleHistory.emit(true);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.collectionData?.currentValue !=
      changes.collectionData?.previousValue
    ) {
      this.initForm(changes.collectionData.currentValue);
      this.uuid = this.collectionData.uuid;
      this.hasApprovalPending =
        this.collectionData.need_to_approve == 'yes' ? true : false;

      this.invoices = this.collectionData.collectiondetails;
      if (this.collectionData.id) {
        this.getDocument('print');
      }
    }
  }
  public ngOnDestroy() {}
  getPaymentName(id) {
    let mode = '';
    switch (parseInt(id)) {
      case 1:
        mode = 'check';
        break;
      case 2:
        mode = 'cash';
        break;
      case 3:
        mode = 'Advance Payment';
      default:
        mode = '';
        break;
    }
    return mode;
  }
  initForm(data) {
    const orgName = localStorage.getItem('org_name');
    const subject = `${orgName} sent you an collection`;
    const message = `${orgName} sent you an collection`;
    this.emailData = {
      email: data.customer.email,
      subject,
      message,
      type: 'collection',
    };
  }
  getDocument = (type) => {
    const model = {
      id: this.collectionData.id,
    };
    if (type == 'pdf') {
      model['status'] = 'pdf';
    }
    this.collectionService.getDocument(model).subscribe((res: any) => {
      if (res.status) {
        if (res.data && res.data.html_string) {
          this.collectionTemplate = this.sanitizer.bypassSecurityTrustHtml(
            res.data.html_string
          );
        } else {
          const link = document.createElement('a');
          link.setAttribute('target', '_blank');
          link.setAttribute('href', `${res.data.file_url}`);
          link.setAttribute('download', `statement.pdf`);
          document.body.appendChild(link);
          link.click();
          link.remove();
        }
      }
    });
  };
  public closeDetailView(): void {
    this.isDetailVisible = false;
    this.detailsClosed.emit();
    this.dataService.sendData({ type: CompDataServiceType.CLOSE_DETAIL_PAGE });
  }
  approve() {
    if (this.collectionData && this.collectionData.objectid) {
      this.apiService
        .approveItem(this.collectionData.objectid)
        .subscribe((res: any) => {
          const approvedStatus: boolean = res.data.approved_or_rejected;
          if (res.status && approvedStatus) {
            this.commonToasterService.showSuccess(
              'Approved',
              'Collection has been Approved'
            );
            this.hasApprovalPending = false;
          }
        });
    }
  }

  reject() {
    if (this.collectionData && this.collectionData.objectid) {
      this.apiService
        .rejectItemApproval(this.collectionData.objectid)
        .subscribe((res: any) => {
          this.commonToasterService.showSuccess(
            'Reject',
            'Collection Approval has been Rejected'
          );
          this.hasApprovalPending = true;
        });
    }
  }
}

const INVOICE_TABLE_HEADERS: InfoModal[] = [
  { id: 0, name: '#' },
  { id: 1, name: 'Date' },
  { id: 2, name: 'Invoice Number' },
  { id: 4, name: 'Total Amount' },
  { id: 5, name: 'Paid Amount' },
];
