import { CommonToasterService } from 'src/app/services/common-toaster.service';
import { Component, ElementRef, OnDestroy, OnInit, Input } from '@angular/core';
import { InfoModal } from '../../collection/collection-models';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { FormDrawerService } from 'src/app/services/form-drawer.service';
import { DataEditor } from 'src/app/services/data-editor.service';
import { Utils } from 'src/app/services/utils';
import { InvoiceServices } from '../invoice.service';
import { CodeDialogComponent } from 'src/app/components/dialogs/code-dialog/code-dialog.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-invoice-collection',
  templateUrl: './invoice-collection.component.html',
  styleUrls: ['./invoice-collection.component.scss']
})

export class InvoiceCollectionComponent implements OnInit, OnDestroy {
  @Input() public invoiceData: any;
  public uuid: string;
  public invoiceitemObject: any;
  public finalInvoicpayload = {};
  public nextCommingNumberofCollectionCode: string = '';
  public payModes: InfoModal[] = [
    { id: 0, name: "Check" },
    { id: 1, name: "Cash" },
    { id: 2, name: "Advance Payment" }
  ];
  public todayDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
  public collectionForm: FormGroup;
  public modeFormControl: FormControl;
  public numberFormControl: FormControl;
  public dateFormControl: FormControl;
  public amountFormControl: FormControl;
  public chequedateFormControl: FormControl;
  public chequenumberFormControl: FormControl;
  public bankFormControl: FormControl;

  private router: Router;
  private fds: FormDrawerService;
  private subscriptions: Subscription[] = [];
  private route: ActivatedRoute;
  private formBuilder: FormBuilder;
  private dialogRef: MatDialog;
  public bankList: any[] = [];
  nextCommingNumberofCollectionCodePrefix: any;

  constructor(
    private invoiceServices: InvoiceServices,
    private datePipe: DatePipe,
    fds: FormDrawerService,
    dataService: DataEditor,
    dialogRef: MatDialog,
    elemRef: ElementRef,
    formBuilder: FormBuilder,
    private ctc: CommonToasterService,
    router: Router,
    route: ActivatedRoute) {
    Object.assign(this, { fds, dataService, dialogRef, elemRef, formBuilder, router, route });
  }

  public ngOnInit(): void {
    this.uuid = this.route.snapshot.params.uuid;
    this.buildForm();
    this.getOrderCode();
    this.invoiceServices.getBankList().subscribe((res: any) => {
      this.bankList = res.data;
    })
  }

  buildForm() {
    this.numberFormControl = this.formBuilder.control('', Validators.required);
    this.dateFormControl = this.formBuilder.control('', Validators.required);
    this.amountFormControl = this.formBuilder.control(+this.invoiceData.grand_total, [Validators.required, Validators.max(+this.invoiceData.grand_total)]);
    this.modeFormControl = this.formBuilder.control(1, Validators.required);
    this.chequedateFormControl = this.formBuilder.control('', Validators.required);
    this.chequenumberFormControl = this.formBuilder.control('', Validators.required);
    this.bankFormControl = this.formBuilder.control('', Validators.required);
    this.collectionForm = this.formBuilder.group({
      payemnt_type: this.modeFormControl,
      collection_number: this.numberFormControl,
      collection_date: this.dateFormControl,
      invoice_amount: this.amountFormControl,
      cheque_number: this.chequenumberFormControl,
      cheque_date: this.chequedateFormControl,
      bank_info: this.bankFormControl
    });
  }

  getOrderCode() {
    let nextNumber = {
      "function_for": "collection"
    }
    this.invoiceServices.nexCommingNumber(nextNumber).subscribe((res: any) => {
      if (res.status) {
        this.nextCommingNumberofCollectionCode = res.data.number_is;
        this.nextCommingNumberofCollectionCodePrefix = res.data.prefix_is;
        if (this.nextCommingNumberofCollectionCode) {
          this.numberFormControl.setValue(this.nextCommingNumberofCollectionCode);
        }
        else if (this.nextCommingNumberofCollectionCode == null) {
          this.nextCommingNumberofCollectionCode = '';
          this.numberFormControl.setValue('');
        }
      }
      else {
        this.nextCommingNumberofCollectionCode = '';
        this.numberFormControl.setValue('');
      }
    });
  }

  public openNumberSettings(): void {
    let data = {
      title: 'Order Code',
      functionFor: 'collection',
      code: this.nextCommingNumberofCollectionCode,
      prefix: this.nextCommingNumberofCollectionCodePrefix,
      key: this.nextCommingNumberofCollectionCode.length
        ? 'autogenerate'
        : 'manual',
    };
    this.dialogRef.open(CodeDialogComponent, {
      width: '500px',
      data: data
    }).componentInstance.sendResponse.subscribe((res: any) => {
      if (res.type == 'manual' && res.enableButton) {
        this.numberFormControl.setValue('');
        this.nextCommingNumberofCollectionCode = '';
      } else if (res.type == 'autogenerate' && !res.enableButton) {
        this.numberFormControl.setValue(res.data.next_coming_number_invoice);
        this.nextCommingNumberofCollectionCode = res.data.next_coming_number_collection;
        this.nextCommingNumberofCollectionCodePrefix = res.reqData.prefix_code;
      }
    });
  }

  public ngOnDestroy() {
    Utils.unsubscribeAll(this.subscriptions);
  }

  public checkFormValidation(): boolean {
    if (this.amountFormControl.invalid) {
      this.ctc.showWarning("Invalid!!!", "Please enter correct amount");
      return false;
    }
    else if (this.numberFormControl.invalid) {
      this.ctc.showWarning("Invalid!!!", "Please enter correct invoice number");
      return false;
    }
    else if (this.dateFormControl.invalid) {
      this.ctc.showWarning("Invalid!!!", "Please enter correct collection date");
      return false;
    }
    else if (this.modeFormControl.invalid) {
      this.ctc.showWarning("Invalid!!!", "Please select correct collection type");
      return false;
    }

    if (this.modeFormControl.value == 0) {
      if (this.chequedateFormControl.invalid) {
        this.ctc.showWarning("Invalid!!!", "Please enter correct check date");
        return false;
      }
      else if (this.chequenumberFormControl.invalid) {
        this.ctc.showWarning("Invalid!!!", "Please enter correct check number");
        return false;
      }
      else if (this.bankFormControl.invalid) {
        this.ctc.showWarning("Invalid!!!", "Please enter correct bank");
        return false;
      }
      return false;
    }
    return true;
  }

  public saveCollection(): void {
    if (!this.checkFormValidation()) {
      return;
    }

    const itemPayload = {
      "invoice_id": this.invoiceData.id,
      "amount": this.amountFormControl.value
    };

    this.invoiceitemObject = { ...itemPayload }

    const finalPayload = {
      "customer_id": this.invoiceData.user?.id,
      "salesman_id": "",
      "collection_type": this.invoiceData?.order_type_id,
      "source": 3,
      "transaction_number": "",
      "status": 1,
      "items": [],
      "current_stage": "pending",
      ...this.collectionForm.value
    };

    let cashPaymentPayloadArray: any[] = [];
    cashPaymentPayloadArray.push(this.invoiceitemObject);
    finalPayload.items = cashPaymentPayloadArray;
    this.finalInvoicpayload = finalPayload;

    this.postCollection();
  }

  public closeForm(): void {
    this.fds.close().then(() => {
      this.fds.setFormName('');
    });
  }

  public postCollection() {
    this.invoiceServices.addCollection(this.finalInvoicpayload).subscribe((res: any) => {
      if (res.status) {
        this.ctc.showSuccess("Colection Added", "YOur Payment has been recorded")
      }
    });
  }
}
