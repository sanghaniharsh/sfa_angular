import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoadRequestMasterComponent } from './load-request-master/load-request-master.component';


const routes: Routes = [
  {
    path: '',
    component: LoadRequestMasterComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoadRequestRoutingModule { }
