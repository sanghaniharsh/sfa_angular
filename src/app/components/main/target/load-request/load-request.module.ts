import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialImportModule } from 'src/app/imports/material-import/material-import.module';
import { SharedModule } from 'src/app/features/shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LightboxModule } from 'ngx-lightbox';

import { LoadRequestRoutingModule } from './load-request-routing.module';
import { LoadRequestMasterComponent } from './load-request-master/load-request-master.component';
import { LoadRequestDtComponent } from './load-request-dt/load-request-dt.component';
import { LoadRequestDetailComponent } from './load-request-detail/load-request-detail.component';


@NgModule({
  declarations: [LoadRequestMasterComponent, LoadRequestDtComponent, LoadRequestDetailComponent],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    LightboxModule,
    ReactiveFormsModule,
    MaterialImportModule,
    LoadRequestRoutingModule
  ]
})
export class LoadRequestModule { }
