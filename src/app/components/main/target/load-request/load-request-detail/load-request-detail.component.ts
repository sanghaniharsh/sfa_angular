import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges } from '@angular/core';
import { DataEditor } from 'src/app/services/data-editor.service';
import { CompDataServiceType } from 'src/app/services/constants';
import { LoadRequest } from '../load-request-interface';
import { Lightbox } from 'ngx-lightbox';
import { TargetService } from '../../target.service';
import { BaseComponent } from 'src/app/features/shared/base/base.component';
import { CommonToasterService } from 'src/app/services/common-toaster.service';
import { DeleteConfirmModalComponent } from 'src/app/components/shared/delete-confirmation-modal/delete-confirmation-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { Utils } from 'src/app/services/utils';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-load-request-detail',
  templateUrl: './load-request-detail.component.html',
  styleUrls: ['./load-request-detail.component.scss']
})
export class LoadRequestDetailComponent extends BaseComponent implements OnInit {
  @Output() public detailsClosed: EventEmitter<any> = new EventEmitter<any>();
  @Input() public loadRequest: LoadRequest | any;
  @Input() public isDetailVisible: boolean;
  @Output() public updateTableData: EventEmitter<any> = new EventEmitter<any>();
  private dataService: DataEditor;
  itemTableHeaders = ['Name', 'UOM', 'Quantity'];
  constructor(private cts: CommonToasterService, private deleteDialog: MatDialog, dataService: DataEditor, private _lightbox: Lightbox, public tService: TargetService) {
    super('load request');
    Object.assign(this, { dataService });
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      if (changes.loadRequest) {
        let currentValue = changes.loadRequest.currentValue;
        this.loadRequest = currentValue;
      }
    }
  }

  rejectLoad(): void {
    this.tService.rejectLoadRequest(this.loadRequest.uuid).subscribe(result => {
      this.cts.showSuccess("", "Rejected Successfully");
    });
  }

  approveLoad(): void {
    this.tService.approveLoadRequest(this.loadRequest.uuid).subscribe(result => {
      this.cts.showSuccess("", "Approved Successfully");
    });
  }

  public openConfirmBox(type): void {
    this.deleteDialog.open(DeleteConfirmModalComponent, {
      width: '500px',
      data: { title: `Are you sure want to ${type} Load Request ${this.loadRequest.load_number}`, btnText: type }
    }).afterClosed().subscribe(data => {
      if (data.hasConfirmed) {
        if (type == 'delete') {
          this.deleteBank();
        } else if (type == 'approve') {
          this.approveLoad();
        } else if (type == 'reject') {
          this.rejectLoad();
        }
      }
    });
  }

  public deleteBank(): void {
    let delObj = { uuid: this.loadRequest.uuid, delete: true };
    this.tService.deleteLoadRequest(this.loadRequest.uuid).subscribe(result => {
      this.closeDetailView();
      this.updateTableData.emit(delObj);
      this.cts.showSuccess("", "Deleted Successfully");

    });
  }

  public closeDetailView(): void {
    this.isDetailVisible = false;
    this.detailsClosed.emit();
    this.dataService.sendData({ type: CompDataServiceType.CLOSE_DETAIL_PAGE });
  }

}
