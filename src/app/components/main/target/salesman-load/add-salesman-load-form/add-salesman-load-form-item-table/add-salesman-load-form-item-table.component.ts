import { Component, OnDestroy, OnInit, Input, OnChanges } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { formatDate } from '@angular/common';
import { Subscription, Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { ApiService } from 'src/app/services/api.service';
import { DataEditor } from 'src/app/services/data-editor.service';
import { Item } from 'src/app/components/main/master/item/item-dt/item-dt.component';
import { Utils } from 'src/app/services/utils';
import { ItemUoms } from 'src/app/components/main/settings/item/item-uom/itemuoms-dt/itemuoms-dt.component';

@Component({
  selector: 'app-add-salesman-load-form-item-table',
  templateUrl: './add-salesman-load-form-item-table.component.html',
  styleUrls: ['./add-salesman-load-form-item-table.component.scss']
})
export class AddSalesmanLoadFormItemTableComponent implements OnInit, OnChanges {
  @Input() salesmanLoadFormGroup: FormGroup;
  @Input() editData: any;
  public pageTitle: string;
  public isEditForm: boolean;
  public uuid: string;

  public itemTableHeaders: any[] = [];

  public items: Item[] = [];
  public filteredItems: Item[] = [];
  public uoms: ItemUoms[] = [];
  public uomArray = [];
  public orderFormGroup: FormGroup;

  private router: Router;
  private apiService: ApiService;
  private dataService: DataEditor;
  private subscriptions: Subscription[] = [];
  private itemNameSubscriptions: Subscription[] = [];
  private itemControlSubscriptions: Subscription[] = [];
  private route: ActivatedRoute;
  private formBuilder: FormBuilder;

  constructor(apiService: ApiService, dataService: DataEditor, formBuilder: FormBuilder, router: Router, route: ActivatedRoute) {
    Object.assign(this, { apiService, dataService, formBuilder, router, route });
  }

  public ngOnInit(): void {

    this.orderFormGroup = new FormGroup({
      'items': this.initItemFormArray()
    });
    this.subscriptions.push(this.apiService.getAllItems().subscribe(result => {
      this.items = result.data;
      this.addItemFilterToControl(0);
      // this.isItemFormReady = true;
    }));
    this.subscriptions.push(this.apiService.getAllItemUoms().subscribe(result => {
      this.uoms = result.data;
    }));
    this.salesmanLoadFormGroup.addControl('items', this.orderFormGroup.controls['items'])
  }
  public ngOnChanges() {
    this.isEditForm = this.router.url.includes('/edit/');
    this.itemTableHeaders = ITEM_ADD_FORM_TABLE_HEADS;
    if (this.editData) {
      const itemControl = this.orderFormGroup.controls['items'] as FormArray;
      itemControl.removeAt(0);
      this.editData.salesman_load_details.forEach((item, index) => {
        let row = this.formBuilder.group({
          'item': [{ id: item.item?.id, name: item.item?.item_name }],
          'uom': [item.item_uom],
          'qty': [item.load_qty],
          'uom_list': []
        })
        // //console.log(row);
        // this.uomArray.push([this.uoms])


        // replace timeout by condition when item isselected
        setTimeout(() => {
          this.onItemSelection({ id: item.item?.id, name: item.item?.item_name }, index);
        }, 1000)

        itemControl.push(row);
      })

      // this.setupEditFormControls(this.editData);
    } else {

    }
  }
  public ngOnDestroy() {
    Utils.unsubscribeAll(this.subscriptions);
    Utils.unsubscribeAll(this.itemNameSubscriptions);
  }
  public setupEditFormControls(editData): void {
    editData.salesman_load_details.forEach((item, index: number) => {
      this.addItemForm(item);
      this.itemDidSearched(item, index, true);
    });
  }
  public addItemForm(item?: any): void {
    const itemControls = this.orderFormGroup.controls['items'] as FormArray;
    if (item) {
      itemControls.push(this.formBuilder.group({
        'item': new FormControl({ id: item.item?.id, name: item.item?.item_name }, [Validators.required]),
        'uom': new FormControl(item.item_uom, [Validators.required]),
        'qty': new FormControl(item.load_qty, [Validators.required]),
        'uom_list': new FormControl([item.uom_info])
      }));
    } else {
      itemControls.push(this.formBuilder.group({
        'item': new FormControl('', [Validators.required]),
        'uom': new FormControl(undefined, [Validators.required]),
        'qty': new FormControl('', [Validators.required]),
        'uom_list': new FormControl([])
      }));
    }

    this.uomArray.push([])
    this.addItemFilterToControl(itemControls.controls.length - 1);
  }
  private initItemFormArray(): FormArray {
    const formArray = this.formBuilder.array([]);

    formArray.push(this.formBuilder.group({
      'item': new FormControl('', [Validators.required]),
      'uom': new FormControl(undefined, [Validators.required]),
      'qty': new FormControl('', [Validators.required]),
      'uom_list': new FormControl([])
    }));
    this.uomArray.push([]);
    return formArray;
  }


  public itemDidSearched(data: any, index: number, isFromEdit?: boolean): void {
    //
    if (isFromEdit) {
      const selectedItem = this.items.find((item: Item) => item.id === data.item_id);
      const itemFormGroup = this.itemFormControls[index] as FormGroup;
      const uomControl = itemFormGroup.controls.uom;
      this.setUpRelatedUom(selectedItem, itemFormGroup);
    }
    else if (!isFromEdit) {

      const selectedItem = this.items.find((item: Item) => item.id === data.id);
      const itemFormGroup = this.itemFormControls[index] as FormGroup;
      const uomControl = itemFormGroup.controls.uom;
      this.setUpRelatedUom(selectedItem, itemFormGroup);
    }

    // uomControl.setValue(selectedItem.lower_unit_uom_id);
    // this.showSpinner = false;
  }

  setUpRelatedUom(selectedItem: any, formGroup: FormGroup) {
    //
    const uomControl = formGroup.controls.uom;
    const baseUomFilter = this.uoms.filter((item) => item.id == parseInt(selectedItem.lower_unit_uom_id));
    if (baseUomFilter.length) {
      formGroup.controls.uom_list.setValue([...baseUomFilter]);
    }
    const secondaryUomFilterIds = [];
    const secondaryUomFilter = [];
    if (selectedItem.item_main_price && selectedItem.item_main_price.length) {
      selectedItem.item_main_price.forEach((item) => {
        secondaryUomFilterIds.push(item.item_uom_id);
      });
      this.uoms.forEach((item) => {
        if (secondaryUomFilterIds.includes(item.id)) {
          secondaryUomFilter.push(item);
        }
      });
      if (secondaryUomFilter.length) {
        formGroup.controls.uom_list.setValue([...baseUomFilter, ...secondaryUomFilter]);
      }
    }
    if (baseUomFilter.length) {
      uomControl.setValue(selectedItem.lower_unit_uom_id);
    }
    else {
      uomControl.setValue(secondaryUomFilter[0].id);
    }
  }




  private addItemFilterToControl(index: number): void {
    const itemControls = this.orderFormGroup.controls['items'] as FormArray;
    const newFormGroup = itemControls.controls[index] as FormGroup;

    this.itemNameSubscriptions.push(newFormGroup.controls['item'].valueChanges.pipe(
      startWith<string | Item>(''),
      map(value => typeof value === 'string' ? value : value.item_name),
      map((value: string) => {
        return value ? this.filterItems(value) : this.items.slice();
      })
    ).subscribe((result: Item[]) => {
      this.filteredItems = result;
    }));

    // this.payloadItems[index] = this.setupPayloadItemArray(newFormGroup);

    this.itemControlSubscriptions.push(newFormGroup.valueChanges.subscribe(result => {
      const groupIndex = itemControls.controls.indexOf(newFormGroup);
      if (newFormGroup.controls['item'].value && newFormGroup.controls['uom'].value) {
        const params: any = {
          item: result.item.id,
          uom: result.uom,
          qty: result.qty
        };
        // this.subscriptions.push(this.apiService.getOrderItemStats(params).subscribe(stats => {
        //   this.payloadItems[groupIndex] = this.setupPayloadItemArray(newFormGroup, stats.data);
        //   this.generateOrderFinalStats();
        // }));
      }
    }));
  }
  public addItem(): void {
    this.addItemForm();
  }
  onItemSelection(event, i) {
    //console.log(event, i);
    let selectedItem;
    let filteredUOMs: any[] = [];
    this.items.forEach(item => {
      if (item.id == event.id) {
        selectedItem = item
      }
    })
    //console.log(selectedItem);
    if (selectedItem) {
      let secondaryUOMs = [];
      selectedItem?.item_main_price.forEach(uom => {
        secondaryUOMs.push(uom.item_uom);
      })
      filteredUOMs = [...filteredUOMs, ...secondaryUOMs]
      filteredUOMs.push(selectedItem?.item_uom_lower_unit)
    }
    this.uomArray[i] = filteredUOMs;

  }
  private filterItems(itemName: string): Item[] {
    const filterValue = itemName.toLowerCase();

    return this.items.filter(item => item.item_name.toLowerCase().includes(filterValue));
  }
  public deleteItemRow(index: number): void {
    const itemControls = this.orderFormGroup.get('items') as FormArray;
    itemControls.removeAt(index);

    this.itemNameSubscriptions.splice(index, 1);
    this.itemControlSubscriptions.splice(index, 1);
    this.uomArray.splice(index, 1);
  }
  public get itemFormControls(): AbstractControl[] {
    const itemControls = this.orderFormGroup.get('items') as FormArray;

    return itemControls.controls;
  }
  public itemsControlDisplayValue(item?: { id: string; name: string; }): string | undefined {
    return item ? item.name : undefined;
  }
  public itemControlValue(item: Item): { id: string; name: string; } {
    return { id: item.id, name: item.item_name };
  }

}

const ITEM_ADD_FORM_TABLE_HEADS: any[] = [
  { id: 0, key: 'sequence', label: '#' },
  { id: 1, key: 'item', label: 'Item Name' },
  { id: 2, key: 'uom', label: 'UOM' },
  { id: 3, key: 'qty', label: 'Quantity' },
];
