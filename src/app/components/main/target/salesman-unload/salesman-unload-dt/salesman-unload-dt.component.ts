import {
  Component,
  OnInit,
  ViewChild,
  Output,
  Input,
  EventEmitter,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { TargetService } from '../../target.service';
import { SelectionModel } from '@angular/cdk/collections';
import { DataEditor } from 'src/app/services/data-editor.service';
import { MatDialog } from '@angular/material/dialog';
import { FormDrawerService } from 'src/app/services/form-drawer.service';
import { Subscription } from 'rxjs';
import { Utils } from 'src/app/services/utils';
import { ColumnConfig } from 'src/app/interfaces/interfaces';
import { CompDataServiceType } from 'src/app/services/constants';
import { EventBusService } from 'src/app/services/event-bus.service';
import { Events } from 'src/app/models/events.model';
import { SalesmanUnload } from '../salesman-unload-interface';
import { PAGE_SIZE_10 } from 'src/app/app.constant';

@Component({
  selector: 'app-salesman-unload-dt',
  templateUrl: './salesman-unload-dt.component.html',
  styleUrls: ['./salesman-unload-dt.component.scss']
})
export class SalesmanUnloadDtComponent implements OnInit {
  @Output() public itemClicked: EventEmitter<any> = new EventEmitter<any>();
  @Input() public isDetailVisible: boolean;
  public dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public selections = new SelectionModel(true, []);
  public displayedColumns: ColumnConfig[] = [];
  public filterColumns: ColumnConfig[] = [];
  private dataEditor: DataEditor;
  private subscriptions: Subscription[] = [];
  public apiResponse = {
    pagination: {
      total_records: 0
    }
  };
  pageSize = PAGE_SIZE_10;
  private allColumns: ColumnConfig[] = [
    { def: 'select', title: 'Select', show: true },
    { def: 'created_at', title: 'Date', show: true },
    { def: 'salesman', title: 'Salesman', show: true },
    { def: 'unload_number', title: 'Unload No.', show: true },
  ];
  private collapsedColumns: ColumnConfig[] = [
    { def: 'select', title: 'Select', show: true },
    { def: 'salesman', title: 'Salesman', show: true },
    { def: 'unload_number', title: 'Unload No.', show: true },
  ];
  dateFilterControl: FormControl;
  constructor(
    public tService: TargetService,
    dataEditor: DataEditor,
    private eventService: EventBusService,
    fds: FormDrawerService,
    deleteDialog: MatDialog
  ) {
    Object.assign(this, { tService, dataEditor, fds, deleteDialog });
    this.dataSource = new MatTableDataSource<SalesmanUnload>();
  }

  public ngOnInit(): void {
    let today = new Date();
    let month = '' + (today.getMonth() + 1);
    let date = '' + today.getDate();
    if (today.getMonth() + 1 < 10) {
      month = '0' + (today.getMonth() + 1);
    }
    if (today.getDate() < 10) {
      date = '0' + today.getDate();
    }
    let newdate = today.getFullYear() + '-' + month + '-' + date;
    this.dateFilterControl = new FormControl(newdate);

    this.displayedColumns = this.allColumns;
    this.filterColumns = [...this.allColumns].splice(1);
    this.getsalesmanUnloadList(1, this.pageSize);

    this.subscriptions.push(
      this.dataEditor.newData.subscribe((value) => {
        if (value.type === CompDataServiceType.CLOSE_DETAIL_PAGE) {
          this.closeDetailView();
        }
      })
    );
    this.subscriptions.push(
      this.eventService.on(Events.SEARCH_COMPETITOR, (data) => {
        this.dataSource = new MatTableDataSource<SalesmanUnload>(data);
        this.dataSource.paginator = this.paginator;
      })
    );
  }

  public getsalesmanUnloadList(page, pageSize) {
    this.subscriptions.push(
      this.tService.getSalesmanUnloadList(page, pageSize).subscribe((res) => {
        this.apiResponse = res;
        this.dataSource = new MatTableDataSource<SalesmanUnload>(res.data);
      })
    );
  }

  onPageFired(data) {
    let page = data['pageIndex'] + 1;
    let pageSize = data['pageSize'];
    this.getsalesmanUnloadList(page, pageSize);
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }

  public openDetailView(data: SalesmanUnload): void {
    this.isDetailVisible = true;
    this.itemClicked.emit(data);
    this.updateCollapsedColumns();
  }

  public closeDetailView(): void {
    this.isDetailVisible = false;
    this.updateCollapsedColumns();
  }

  private updateCollapsedColumns(): void {
    this.displayedColumns = this.isDetailVisible
      ? this.collapsedColumns
      : this.allColumns;
  }

  public getDisplayedColumns(): string[] {
    return this.displayedColumns
      .filter((column) => column.show)
      .map((column) => column.def);
  }

  public isAllSelected(): boolean {
    return this.selections.selected.length === this.dataSource.data.length;
  }

  public toggleSelection(): void {
    this.isAllSelected()
      ? this.selections.clear()
      : this.dataSource.data.forEach((row) => this.selections.select(row));
  }

  public checkboxLabel(row?: SalesmanUnload): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selections.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1
      }`;
  }

  getPaginatorValue(len: number) {
    return len < 10 ? true : false;
  }

}
