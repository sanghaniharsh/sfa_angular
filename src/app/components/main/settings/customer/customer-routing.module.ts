import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { OutletProductPageComponent } from './outlet-product-code/outlet-product-page/outlet-product-page.component';

const routes: Routes = [
  { path: 'outlet-product-code', component: OutletProductPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerRoutingModule {}
