import { Subscription } from 'rxjs';
import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  Input,
  ViewChild,
  AfterViewInit,
} from '@angular/core';
import { FormDrawerService } from 'src/app/services/form-drawer.service';
import { DataEditor } from 'src/app/services/data-editor.service';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { CompDataServiceType } from 'src/app/services/constants';
import { CommonModelService } from 'src/app/services/common-model.service';
import { Warehouse } from '../warehouse-dt/warehouse-dt.component';
import { DeleteConfirmModalComponent } from 'src/app/components/shared/delete-confirmation-modal/delete-confirmation-modal.component';
import { AddStockDialogComponent } from 'src/app/components/dialogs/add-stock-dialog/add-stock-dialog.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { CommonToasterService } from 'src/app/services/common-toaster.service';

@Component({
  selector: 'app-warehouse-detail-page',
  templateUrl: './warehouse-detail-page.component.html',
  styleUrls: ['./warehouse-detail-page.component.scss'],
})
export class WarehouseDetailPageComponent implements OnInit, AfterViewInit {
  @Output() public detailsClosed: EventEmitter<any> = new EventEmitter<any>();
  @Output() public updateTableData: EventEmitter<any> = new EventEmitter<any>();
  @Input() public warehouse: Warehouse | any;
  @Input() public isDetailVisible: boolean;
  public stockData: any[] = [];
  public subscriptions: Subscription[] = [];
  public dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  public currentIndex: number = 0;
  public displayedColumns = ['item_code', 'item_name', 'name', 'batch', 'qty'];

  private dataService: DataEditor;
  private formDrawer: FormDrawerService;
  private deleteDialog: MatDialog;
  private apiService: ApiService;

  constructor(
    apiService: ApiService,
    deleteDialog: MatDialog,
    dataService: DataEditor,
    formDrawer: FormDrawerService,
    private cts: CommonToasterService
  ) {
    Object.assign(this, { apiService, deleteDialog, dataService, formDrawer });
    this.dataSource = new MatTableDataSource<Warehouse>();
  }

  ngOnInit(): void {
    this.dataService.nextSubjectDataObj.subscribe((res: any) => {
      if (JSON.stringify(res) != '{}') {
        this.dataSource.data = res;
        this.dataSource.paginator = this.paginator;
      }
    });
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }
  public closeDetailView(): void {
    this.isDetailVisible = false;
    this.detailsClosed.emit();
    this.dataService.sendData({ type: CompDataServiceType.CLOSE_DETAIL_PAGE });
  }

  public openEditWarehouse(): void {
    this.formDrawer.setFormName('warehouse');
    this.formDrawer.setFormType('Edit');
    this.formDrawer.open();
    this.dataService.sendData({
      type: CompDataServiceType.DATA_EDIT_FORM,
      data: this.warehouse,
    });
  }

  public toggleStatus(): void {
    this.warehouse.vendor = this.warehouse.status === 0 ? 1 : 0;
  }

  public openDeleteBox(): void {
    this.deleteDialog
      .open(DeleteConfirmModalComponent, {
        width: '500px',
        data: { title: `Are you sure want to delete  ${this.warehouse.code}` },
      })
      .afterClosed()
      .subscribe((data) => {
        if (data.hasConfirmed) {
          this.deletewarehouse();
        }
      });
  }

  public deletewarehouse(): void {
    let delObj = { uuid: this.warehouse.uuid, delete: true };
    this.apiService.deletewarehouse(this.warehouse.uuid).subscribe((result) => {
      this.closeDetailView();
      this.updateTableData.emit(delObj);
      this.cts.showSuccess("", "Deleted Successfully");
    });
  }

  public addStock() {
    this.deleteDialog
      .open(AddStockDialogComponent, {
        width: '800px',
        height: 'auto',
        hasBackdrop: true,
        position: {
          top: '5px',
        },
        data: this.warehouse,
      })
      .afterClosed()
      .subscribe((data) => {
        if (data && data.length > 0) {
          this.dataSource.data = data;
          this.dataSource.paginator = this.paginator;
        }
      });
  }
}
