import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormDrawerService } from 'src/app/services/form-drawer.service';
import { ApiService } from 'src/app/services/api.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DataEditor } from 'src/app/services/data-editor.service';
import { Subscription } from 'rxjs';
import { Utils } from 'src/app/services/utils';
import { MatDialog } from '@angular/material/dialog';
import { CommonToasterService } from 'src/app/services/common-toaster.service';
import { SalesMan } from '../salesman-dt/salesman-dt.component';
import { CodeDialogComponent } from 'src/app/components/dialogs/code-dialog/code-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { APP } from 'src/app/app.constant';

@Component({
  selector: 'app-add-salesman-form',
  templateUrl: './add-salesman-form.component.html',
  styleUrls: ['./add-salesman-form.component.scss'],
})
export class AddSalesmanFormComponent implements OnInit {
  @Output() public updateTableData: EventEmitter<any> = new EventEmitter<any>();
  domain = window.location.host.split('.')[0];
  public salesManFormGroup: FormGroup;
  public imageFormControl: FormControl;
  public salesManCodeFormControl: FormControl;
  public salesManFirstNameFormControl: FormControl;
  public salesManLastNameFormControl: FormControl;
  public salesManTypeFormControl: FormControl;
  public salesManRouteIdFormControl: FormControl;
  nextCommingNumberofsalesmanCode: string = '';
  // public userTypeFormControl: FormControl;
  //public salesManParentIdFormControl: FormControl;
  public salesManEmailFormControl: FormControl;
  public salesManPasswordFormControl: FormControl;
  //public salesManCountryIdFormControl: FormControl;
  //public isApprovedByAdminFormControl: FormControl;
  // public roleIdFormControl: FormControl;
  public salesManRoleFormControl: FormControl;
  public salesManSupervisorFormControl: FormControl;
  public salesManMobileFormControl: FormControl;
  public oderFromFormControl: FormControl;
  public orderToFormControl: FormControl;
  public customerFromFormControl: FormControl;
  public customerToFormControl: FormControl;
  public invoiceFromFormControl: FormControl;
  public invoiceToFormControl: FormControl;
  public returnFromFormControl: FormControl;
  public returnToFormControl: FormControl;
  public collectionFromFormControl: FormControl;
  public collectionToFormControl: FormControl;
  public unloadFromFormControl: FormControl;
  public unloadToFormControl: FormControl;
  public salesManData: SalesMan;
  public salesmanRoles: any[];
  public salesmanRolesByType = [];
  public formType: string;
  public togglePassword: boolean = true;
  private isEdit: boolean = false;
  private fds: FormDrawerService;
  private apiService: ApiService;
  private dataEditor: DataEditor;
  private subscriptions: Subscription[] = [];
  areas: any[] = [];
  public customerID: any[] = [];
  formPopulateData: any;

  isCustomField = false;
  moduleId = APP.MODULE.SALESMAN;
  customFields: Array<any> = [];
  editData = [];
  public selectedFiles = [];
  public filechoosed = false;
  nextCommingNumberofsalesmanCodePrefix: any;

  constructor(
    private route: ActivatedRoute,
    fds: FormDrawerService,
    apiService: ApiService,
    dataEditor: DataEditor,
    public dialog: MatDialog,
    private commonToasterService: CommonToasterService
  ) {
    Object.assign(this, { fds, apiService, dataEditor });
  }

  public ngOnInit(): void {
    this.buildForm();
    this.getCustomFieldStatus();
    this.formPopulateData = this.route.snapshot.data[
      'salesman_resolve'
    ].salesmanAdd.data;
    this.loadFormData();
    this.fds.formType.subscribe((s) => {
      this.formType = s;
      if (this.formType == 'Add') {
        this.salesManFormGroup.reset();
        this.getNextComingCode();
        this.initializeNumberRange();
        this.isEdit = false;
      } else if (this.formType == 'Edit') {
        this.salesManFormGroup.reset();
        this.initializeNumberRange();
        this.isEdit = true;
      }
    });

    this.subscriptions.push(
      this.dataEditor.newData.subscribe((result) => {
        const data: SalesMan = result.data;
        if (data && data.uuid && this.isEdit) {
          this.editData = result.data.custom_field_value_save;
          this.salesManCodeFormControl.setValue(
            data.salesman_code ? data.salesman_code : ''
          );
          this.salesManCodeFormControl.disable();
          this.salesManFirstNameFormControl.setValue(
            data.user ? data.user.firstname : ''
          );
          this.salesManLastNameFormControl.setValue(
            data.user ? data.user.lastname : ''
          );
          this.imageFormControl.setValue(
            data.profile_image ? data.profile_image : ''
          );
          if (data.profile_image !== "" && data.profile_image !== null) {
            this.selectedFiles.push(data.profile_image);
          }
          this.salesManTypeFormControl.setValue(data.salesman_type_id);

          this.salesManEmailFormControl.setValue(
            data.user ? data.user.email : ''
          );
          if (this.salesManEmailFormControl.value != '') {
            this.salesManEmailFormControl.disable();
          } else {
            this.salesManEmailFormControl.enable();
          }
          // this.salesManPasswordFormControl.setValue(data.password ? data.password : '');
          this.salesManPasswordFormControl.setErrors(null);
          this.salesManRouteIdFormControl.setValue(
            data.route ? data.route.id : 0
          );
          this.onChangeSalesmanType(data.salesman_type_id);
          this.salesManRoleFormControl.setValue(data.salesman_role_id);
          this.salesManSupervisorFormControl.setValue(
            data.salesman_supervisor ? data.salesman_supervisor : ''
          );

          this.salesManMobileFormControl.setValue(
            data.user ? data.user.mobile : ''
          );
          this.customerToFormControl.setValue(
            data.salesman_range?.customer_to
              ? data.salesman_range?.customer_to
              : ''
          );
          this.customerFromFormControl.setValue(
            data.salesman_range?.customer_from
              ? data.salesman_range?.customer_from
              : ''
          );
          this.orderToFormControl.setValue(
            data.salesman_range?.order_to ? data.salesman_range?.order_to : ''
          );
          this.oderFromFormControl.setValue(
            data.salesman_range?.order_from ? data.salesman_range?.order_from : ''
          );
          this.invoiceFromFormControl.setValue(
            data.salesman_range?.invoice_to ? data.salesman_range?.invoice_to : ''
          );
          this.invoiceToFormControl.setValue(
            data.salesman_range?.invoice_from
              ? data.salesman_range?.invoice_from
              : ''
          );
          this.unloadFromFormControl.setValue(
            data.salesman_range?.unload_from
              ? data.salesman_range?.unload_from
              : ''
          );
          this.unloadToFormControl.setValue(
            data.salesman_range?.unload_to ? data.salesman_range?.unload_to : ''
          );
          this.collectionFromFormControl.setValue(
            data.salesman_range?.collection_from
              ? data.salesman_range?.collection_from
              : ''
          );
          this.collectionToFormControl.setValue(
            data.salesman_range?.collection_from
              ? data.salesman_range?.collection_from
              : ''
          );
          this.returnFromFormControl.setValue(
            data.salesman_range?.credit_note_from
              ? data.salesman_range?.credit_note_from
              : ''
          );
          this.returnToFormControl.setValue(
            data.salesman_range?.credit_note_to
              ? data.salesman_range?.credit_note_to
              : ''
          );
          this.salesManData = data;
          this.isEdit = true;
        }
        return;
      })
    );
  }
  restrictLength(e) {
    if (e.target.value.length >= 10) {
      e.preventDefault();
    }
  }

  fileChosen(event) {
    let files = [];
    if (event.target.files && event.target.files[0]) {
      let filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        const reader = new FileReader();
        reader.onload = (event: any) => {
          files.push(event.target.result);
        };

        reader.readAsDataURL(event.target.files[i]);
      }
      this.selectedFiles = files;
      this.filechoosed = true;
    }
  }

  buildForm() {
    this.imageFormControl = new FormControl('');
    this.salesManCodeFormControl = new FormControl('', [Validators.required]);
    this.salesManFirstNameFormControl = new FormControl('', [
      Validators.required,
    ]);
    this.salesManLastNameFormControl = new FormControl('', [
      Validators.required,
    ]);
    this.salesManTypeFormControl = new FormControl('', [Validators.required]);
    this.salesManEmailFormControl = new FormControl('', [
      Validators.required,
      Validators.email,
    ]);
    this.salesManPasswordFormControl = new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]);
    this.salesManRouteIdFormControl = new FormControl('', [
      Validators.required,
    ]);
    this.salesManRoleFormControl = new FormControl('', [Validators.required]);
    this.salesManSupervisorFormControl = new FormControl('');
    this.salesManMobileFormControl = new FormControl('', [
      Validators.maxLength(13),
    ]);
    this.customerFromFormControl = new FormControl('');
    this.customerToFormControl = new FormControl('');
    this.oderFromFormControl = new FormControl('');
    this.orderToFormControl = new FormControl('');
    this.invoiceFromFormControl = new FormControl('');
    this.invoiceToFormControl = new FormControl('');
    this.returnFromFormControl = new FormControl('');
    this.returnToFormControl = new FormControl('');
    this.collectionFromFormControl = new FormControl('');
    this.collectionToFormControl = new FormControl('');
    this.unloadFromFormControl = new FormControl('');
    this.unloadToFormControl = new FormControl('');

    this.salesManFormGroup = new FormGroup({
      salesManFirstName: this.salesManFirstNameFormControl,
      salesManLastName: this.salesManLastNameFormControl,
      salesManType: this.salesManTypeFormControl,
      salesManCode: this.salesManCodeFormControl,
      salesManEmail: this.salesManEmailFormControl,
      salesManPassword: this.salesManPasswordFormControl,
      salesManRouteId: this.salesManRouteIdFormControl,
      salesManRole: this.salesManRoleFormControl,
      salesManSupervisor: this.salesManSupervisorFormControl,
      mobile: this.salesManMobileFormControl,
      // orderTo:this.orderToFormControl,
      // orderFrom:this.oderFromFormControl,
      // invoiceTo:this.invoiceToFormControl,
      // invoiceFrom:this.invoiceFromFormControl,
      // retrunTo:this.returnToFormControl,
      // returnFrom:this.returnFromFormControl,
      // collectionTo:this.collectionToFormControl,
      // collectionForm:this.collectionFromFormControl,
      // unloadTo:this.unloadToFormControl,
      // unloadFrom:this.unloadFromFormControl
    });
    this.salesManCodeFormControl.disable();
  }
  getNextComingCode() {
    let nextNumber = {
      function_for: 'salesman',
    };
    this.apiService.getNextCommingCode(nextNumber).subscribe((res: any) => {
      if (res.status) {
        this.setItemCode(res.data);
      }
    });
  }
  setItemCode(code: any) {
    if (code.number_is !== null) {
      this.nextCommingNumberofsalesmanCode = code.number_is;
      this.nextCommingNumberofsalesmanCodePrefix = code.prefix_is;
      this.salesManCodeFormControl.setValue(
        this.nextCommingNumberofsalesmanCode
      );
      this.salesManCodeFormControl.disable();
    } else {
      this.nextCommingNumberofsalesmanCode = '';
      this.salesManCodeFormControl.enable();
    }
  }

  loadFormData() {
    const formData = this.formPopulateData;
    this.areas = formData.route;
    this.salesmanRoles = formData.salesman_role;
    this.customerID = formData.salesman_type;
  }

  public close() {
    this.fds.close();
    this.filechoosed = false;
    this.selectedFiles = [];
    this.isEdit = false;
  }
  get isCustomerRange(): boolean {
    if (
      this.customerToFormControl.value?.length &&
      this.customerFromFormControl.value?.length
    ) {
      return true;
    } else {
      return false;
    }
  }

  get isOrderRange(): boolean {
    if (
      this.orderToFormControl.value?.length &&
      this.oderFromFormControl.value?.length
    ) {
      return true;
    } else {
      return false;
    }
  }
  get isInvoiceRange(): boolean {
    if (
      this.invoiceToFormControl.value?.length &&
      this.invoiceFromFormControl.value?.length
    ) {
      return true;
    } else {
      return false;
    }
  }
  get isCollectionRange(): boolean {
    if (
      this.collectionToFormControl.value?.length &&
      this.collectionFromFormControl.value?.length
    ) {
      return true;
    } else {
      return false;
    }
  }
  get isReturnRange(): boolean {
    if (
      this.returnToFormControl.value?.length &&
      this.returnFromFormControl.value?.length
    ) {
      return true;
    } else {
      return false;
    }
  }
  get isUnloadRange(): boolean {
    if (
      this.unloadToFormControl.value?.length &&
      this.unloadFromFormControl.value?.length
    ) {
      return true;
    } else {
      return false;
    }
  }

  onChangeSalesmanType(value) {
    //console.log(value);
    if (value == 2) {
      this.salesmanRolesByType = [];
      this.salesmanRolesByType = this.salesmanRoles.filter((role) =>
        role?.name.toLowerCase().includes('merchandiser')
      );
      this.salesManRouteIdFormControl.setErrors(null);
    } else {
      this.salesmanRolesByType = [];
      this.salesmanRolesByType = this.salesmanRoles.filter(
        (role) => !role?.name.toLowerCase().includes('merchandiser')
      );
      this.salesManRouteIdFormControl.setErrors({ required: true });
    }
    //console.log(this.salesManFormGroup, this.salesManRouteIdFormControl);
  }

  private initializeNumberRange() {
    this.customerToFormControl.reset();
    this.customerFromFormControl.reset();
    this.orderToFormControl.reset();
    this.oderFromFormControl.reset();
    this.invoiceFromFormControl.reset();
    this.invoiceToFormControl.reset();
    this.collectionToFormControl.reset();
    this.collectionFromFormControl.reset();
    this.returnFromFormControl.reset();
    this.returnToFormControl.reset();
    this.unloadToFormControl.reset();
    this.unloadFromFormControl.reset();
  }

  onCustomFieldUpdated(item) {
    if (Array.isArray(item)) {
      this.customFields = item;
    }
  }
  getCustomFieldStatus() {
    this.apiService
      .checkCustomFieldStatus({
        organisation_id: APP.ORGANIZATION,
        module_id: this.moduleId,
      })
      .subscribe((response) => {
        this.isCustomField =
          response.data.custom_field_status == 0 ? false : true;
      });
  }
  validateCustomFields() {
    let isValid;
    const modules = this.customFields.map((item) => {
      const value =
        item.fieldType == 'multi_select'
          ? item.fieldValue.toString()
          : item.fieldValue;
      return {
        module_id: item.moduleId,
        custom_field_id: item.id,
        custom_field_value: value,
      };
    });
    isValid = modules.find(
      (module) =>
        module.custom_field_value === undefined ||
        module.custom_field_value === ''
    );
    if (isValid) {
      this.commonToasterService.showWarning(
        'Warning',
        'Please fill all custom fields.'
      );
      return false;
    }
    return modules;
  }

  public saveSalesManData(): void {
    //console.log(
    // this.salesManFormGroup,
    //   this.salesManFormGroup.invalid,
    //   this.isCustomerRange,
    //   this.isOrderRange,
    //   this.isInvoiceRange,
    //   this.isReturnRange,
    //   this.isUnloadRange,
    //   this.isCollectionRange
    // );
    if (
      this.salesManFormGroup.invalid ||
      !(
        this.isCustomerRange &&
        this.isOrderRange &&
        this.isInvoiceRange &&
        this.isReturnRange &&
        this.isUnloadRange &&
        this.isCollectionRange
      )
    ) {
      return;
    }
    if (this.isEdit) {
      this.editSalesManData();
    } else {
      this.postSalesManData();
    }
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }

  private postSalesManData(): void {
    const modules = this.validateCustomFields();
    if (!modules) return;

    this.apiService
      .addSalesMan({
        usertype: '1',
        parent_id: '',
        firstname: this.salesManFirstNameFormControl.value,
        lastname: this.salesManLastNameFormControl.value,
        email: this.salesManEmailFormControl.value,
        password: this.salesManPasswordFormControl.value,
        mobile: this.salesManMobileFormControl.value,
        country_id: 1,
        is_approved_by_admin: '1',
        role_id: 1,
        status: '1',
        route_id: this.salesManRouteIdFormControl.value || 0,
        salesman_code: this.salesManCodeFormControl.value,
        salesman_supervisor: this.salesManSupervisorFormControl.value,
        salesman_role_id: this.salesManRoleFormControl.value,
        salesman_type_id: this.salesManTypeFormControl.value,
        customer_from: this.customerFromFormControl.value,
        customer_to: this.customerToFormControl.value,
        order_from: this.oderFromFormControl.value,
        order_to: this.orderToFormControl.value,
        invoice_from: this.invoiceFromFormControl.value,
        invoice_to: this.invoiceToFormControl.value,
        collection_from: this.collectionFromFormControl.value,
        collection_to: this.collectionToFormControl.value,
        credit_note_from: this.returnFromFormControl.value,
        credit_note_to: this.returnToFormControl.value,
        unload_from: this.unloadFromFormControl.value,
        unload_to: this.unloadToFormControl.value,
        salesman_profile: this.filechoosed == true ? this.selectedFiles[0] : undefined,
        modules,
      })
      .subscribe(
        (result: any) => {
          this.commonToasterService.showSuccess(
            'Added',
            'Salesman Added Successfully'
          );
          let data = result.data;
          data.edit = false;
          this.updateTableData.emit(data);
          this.fds.close();
        },
        (err) => {
          if (err.error?.errors?.email) {
            this.commonToasterService.showError(
              'Error:',
              err.error?.errors?.email[0]
            );
          }
        }
      );
  }

  private editSalesManData(): void {
    const modules = this.validateCustomFields();
    if (!modules) return;

    this.apiService
      .editSalesMan(this.salesManData.user?.uuid, {
        usertype: '1',
        parent_id: '',
        firstname: this.salesManFirstNameFormControl.value,
        lastname: this.salesManLastNameFormControl.value,
        email: this.salesManEmailFormControl.value,
        password: this.salesManPasswordFormControl.value,
        mobile: this.salesManMobileFormControl.value,
        route_id: this.salesManRouteIdFormControl.value || 0,
        country_id: 1,
        is_approved_by_admin: '1',
        role_id: 1,
        status: '1',
        route_name: this.salesManRouteIdFormControl.value,
        salesman_code: this.salesManCodeFormControl.value,
        salesman_supervisor: this.salesManSupervisorFormControl.value,
        salesman_role_id: this.salesManRoleFormControl.value,
        salesman_type_id: this.salesManTypeFormControl.value,
        customer_from: this.customerFromFormControl.value,
        customer_to: this.customerToFormControl.value,
        order_from: this.oderFromFormControl.value,
        order_to: this.orderToFormControl.value,
        invoice_from: this.invoiceFromFormControl.value,
        invoice_to: this.invoiceToFormControl.value,
        collection_from: this.collectionFromFormControl.value,
        collection_to: this.collectionToFormControl.value,
        credit_note_from: this.returnFromFormControl.value,
        credit_note_to: this.returnToFormControl.value,
        unload_from: this.unloadFromFormControl.value,
        unload_to: this.unloadToFormControl.value,
        salesman_profile: this.filechoosed == true ? this.selectedFiles[0] : undefined,
        modules,
      })
      .subscribe(
        (result: any) => {
          this.isEdit = false;
          this.commonToasterService.showSuccess(
            'Updated',
            'Salesman Updated Successfully'
          );
          let data = result.data;
          data.edit = true;
          this.updateTableData.emit(data);
          this.fds.close();
        },
        (err) => {
          if (err.error?.errors?.email) {
            this.commonToasterService.showError(
              'Error:',
              err.error?.errors?.email[0]
            );
          }
        }
      );
  }

  open() {
    let response: any;
    let data = {
      title: 'Salesman Code',
      functionFor: 'salesman',
      code: this.nextCommingNumberofsalesmanCode,
      prefix: this.nextCommingNumberofsalesmanCodePrefix,
      key: this.nextCommingNumberofsalesmanCode.length
        ? 'autogenerate'
        : 'manual',
    };
    this.dialog
      .open(CodeDialogComponent, {
        width: '500px',
        height: 'auto',
        data: data,
      })
      .componentInstance.sendResponse.subscribe((res: any) => {
        response = res;
        if (res.type == 'manual' && res.enableButton) {
          this.salesManCodeFormControl.setValue('');
          this.nextCommingNumberofsalesmanCode = '';
          this.salesManCodeFormControl.enable();
        } else if (res.type == 'autogenerate' && !res.enableButton) {
          this.salesManCodeFormControl.setValue(
            res.data.next_coming_number_salesman
          );
          this.nextCommingNumberofsalesmanCode =
            res.data.next_coming_number_salesman;
          this.nextCommingNumberofsalesmanCodePrefix = res.reqData.prefix_code;
          this.salesManCodeFormControl.disable();
        }
      });
  }
}
