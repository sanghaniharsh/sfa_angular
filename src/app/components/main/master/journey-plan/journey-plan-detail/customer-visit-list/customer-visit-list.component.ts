import { Component, OnInit, OnChanges, Input, ViewChild, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { MasterService } from '../../../master.service';
import { Utils } from 'src/app/services/utils';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { PAGE_SIZE_10 } from 'src/app/app.constant';
@Component({
  selector: 'app-customer-visit-list',
  templateUrl: './customer-visit-list.component.html',
  styleUrls: ['./customer-visit-list.component.scss']
})
export class CustomerVisitListComponent implements OnInit {
  @Output() public jpListHandler: EventEmitter<any> = new EventEmitter<any>();
  @Input() public listData;
  @Input() public journeyId;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  itemSource = new MatTableDataSource();
  private subscriptions: Subscription[] = [];
  pageSize = PAGE_SIZE_10;
  public displayedColumns = ['created_at', 'customer', 'start_time', 'end_time', 'longitude', 'latitude', 'is_sequnece', 'shop_status', 'reason', 'comment'];

  public allResData = [];
  public apiResponse = {
    pagination: {
      total_records: 0
    }
  };
  filterType: any;
  startDateFilterControl: FormControl;
  endDateFilterControl: FormControl;
  constructor(private mService: MasterService) {
    this.itemSource = new MatTableDataSource<any>();
  }

  ngOnInit(): void {
    let today = new Date();
    let month = '' + (today.getMonth() + 1);
    let date = '' + (today.getDate());
    if ((today.getMonth() + 1) < 10) {
      month = '0' + (today.getMonth() + 1);
    }
    if ((today.getDate()) < 10) {
      date = '0' + (today.getDate());
    }
    let newdate = today.getFullYear() + '-' + month + '-' + date;
    this.startDateFilterControl = new FormControl(newdate);
    this.endDateFilterControl = new FormControl(newdate);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      if (changes.listData) {
        let currentValue = changes.listData.currentValue;
        this.listData = currentValue;
        this.apiResponse = this.listData;
        this.allResData = this.listData.data;
        this.itemSource = new MatTableDataSource<any>(this.listData.data);
      }
    }
  }

  onPageFired(data) {
    let page = data['pageIndex'] + 1;
    let pageSize = data['pageSize'];
    this.getCustomerVisitList(this.filterType, page, pageSize);
  }

  getCustomerVisitList(type, page = 1, pageSize = this.pageSize) {
    let obj = {
      "journey_plan_id": this.journeyId,
      "page": page,
      "page_size": pageSize
    };
    this.filterType = type;
    if (type == 'date') {
      obj["start_date"] = this.startDateFilterControl.value;
      obj["end_date"] = this.endDateFilterControl.value;
    } else {
      obj["all"] = true;
    }

    this.subscriptions.push(
      this.mService.getCustomerVisitList(obj).subscribe((res) => {
        this.apiResponse = res;
        this.allResData = res.data;
        this.listData = res.data;
        this.itemSource = new MatTableDataSource<any>(res.data);

      })
    )
  }

  openCustomerActivityList(data) {
    data.actionType = "activity-list";
    this.jpListHandler.emit(data);
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }

}
