import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { JourneyPlanCustomerModel, JourneyPlanDayModel, JourneyPlanWeekModel } from '../../journey-plan-model';

@Component({
  selector: 'app-jp-detail-customer-table',
  templateUrl: './jp-detail-customer-table.component.html',
  styleUrls: ['./jp-detail-customer-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JpDetailCustomerTableComponent implements OnInit, OnChanges {
  @Input() public customersData: JourneyPlanWeekModel[] | JourneyPlanDayModel[];
  @Input() public isWeekly: boolean;
  @Input() public journeyId: number;

  public hasNewChanges: boolean;
  public tableHeads = ['Sequence', 'Name', 'Start Time', 'End Date'];

  private changeDetectorRef: ChangeDetectorRef;

  constructor(changeDetectorRef: ChangeDetectorRef) {
    Object.assign(this, { changeDetectorRef });
  }

  public ngOnInit(): void {
  }

  public ngOnChanges(changes: SimpleChanges) {
    this.hasNewChanges = false;
    this.changeDetectorRef.detectChanges();

    if (changes.customersData && changes.customersData.currentValue) {
      this.hasNewChanges = true;

      if (this.isWeekly) {
        this.customersData.sort((item1, item2) => {
          const charItem1 = Number(item1.week_number.charAt(item1.week_number.length - 1));
          const charItem2 = Number(item2.week_number.charAt(item2.week_number.length - 1));

          return charItem1 - charItem2;
        });
      }

      this.changeDetectorRef.detectChanges();
    }
  }

  public getWeekNameLabel(weekName: string): string {
    const weekNumber = weekName.charAt(weekName.length - 1);

    return `Week ${weekNumber}`;
  }

  public getUsersName(customer: JourneyPlanCustomerModel): string {
    if (customer.customer_info) {
      return `${customer.customer_info.user.firstname} ${customer.customer_info.user.lastname}`;
    }
    return 'No name';
  }
}
