import { Component, OnInit, Input } from '@angular/core';
import { Customer } from '../../../customer-dt/customer-dt.component';

@Component({
  selector: 'app-overview-left-pannel',
  templateUrl: './overview-left-pannel.component.html',
  styleUrls: ['./overview-left-pannel.component.scss'],
})
export class OverviewLeftPannelComponent implements OnInit {
  public showCollapsedAttr = true;
  public showCollapsedAddr = true;
  constructor() { }
  @Input() public customer: any;

  ngOnInit(): void {
    //console.log(this.customer);
  }
}
