import { Component, OnInit, OnChanges, Input, ViewChild, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { MerchandisingService } from '../../../merchandising.service';
import { Utils } from 'src/app/services/utils';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.scss']
})
export class SurveyListComponent implements OnInit {
  @Output() public surveyHandler: EventEmitter<any> = new EventEmitter<any>();
  @Input() public surveyData;
  @Input() public AssetTrack_id;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  itemSource = new MatTableDataSource();
  private subscriptions: Subscription[] = [];
  public survey_id;
  public surveyView = 1;
  public displayedColumns = ['created_at', 'name', 'start_date', 'end_date', 'actions'];
  dateFilterControl: FormControl;
  constructor(private merService: MerchandisingService) {
    this.itemSource = new MatTableDataSource<any>();
  }

  ngOnInit(): void {
    let today = new Date();
    let month = '' + (today.getMonth() + 1);
    let date = '' + (today.getDate());
    if ((today.getMonth() + 1) < 10) {
      month = '0' + (today.getMonth() + 1);
    }
    if ((today.getDate()) < 10) {
      date = '0' + (today.getDate());
    }
    let newdate = today.getFullYear() + '-' + month + '-' + date;
    this.dateFilterControl = new FormControl(newdate);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      if (changes.surveyData) {
        let currentValue = changes.surveyData.currentValue;
        this.surveyData = currentValue;
        this.itemSource = new MatTableDataSource<any>(this.surveyData);
        this.itemSource.paginator = this.paginator;
      }
    }
    this.surveyView = 1;
  }

  getSurveyList(filter, value) {
    if (filter == "date") {
      value = this.dateFilterControl.value;
    }
    if (value == "") return false;
    this.subscriptions.push(
      this.merService.getAssetTrackSurveyList(this.AssetTrack_id, filter, value).subscribe((res) => {
        this.itemSource = new MatTableDataSource<any>(res.data);

      })
    )
  }

  editSruvey(data) {
    data.actionType = 'edit';
    this.surveyHandler.emit(data);
  }

  ngAfterViewInit() {
    this.itemSource.paginator = this.paginator;
  }

  showSurveyPostList(data) {
    this.survey_id = data.id;
    data.actionType = 'post_list';
    this.surveyHandler.emit(data);
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }

  openSurveyDetailView(data) {
    data.actionType = 'preview';
    this.surveyHandler.emit(data);
  }

  public hidePaginator(len: any): boolean {
    return len < 6 ? true : false;
  }

}
