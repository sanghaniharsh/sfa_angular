import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-market-promo-master',
  templateUrl: './market-promo-master.component.html',
  styleUrls: ['./market-promo-master.component.scss']
})
export class MarketPromoMasterComponent implements OnInit {
  public isDetailVisible: boolean;
  public newMarketPromoData = {};
  public marketPromo = {};
  checkedRows = [];
  constructor() {
  }

  ngOnInit(): void {
  }

  public itemClicked(data: any): void {
    if (data) {
      // this.isDetailVisible = true;
      this.marketPromo = data;
    }
  }

  updateTableData(data) {
    //console.log(data);
    this.newMarketPromoData = data;
  }

  selectedRows(data: any) {
    this.checkedRows = data;
  }

}
