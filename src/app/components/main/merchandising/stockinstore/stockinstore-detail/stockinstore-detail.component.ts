
import { Component, OnInit, Output, EventEmitter, Input, ViewChild, SimpleChanges } from '@angular/core';
import { DataEditor } from 'src/app/services/data-editor.service';
import { FormDrawerService } from 'src/app/services/form-drawer.service';
import { MatDialog } from '@angular/material/dialog';
import { MerchandisingService } from '../../merchandising.service';
import { CompDataServiceType } from 'src/app/services/constants';
import { Stockinstore } from '../stockinstore-interface';
import { CommonToasterService } from 'src/app/services/common-toaster.service';
import { DeleteConfirmModalComponent } from 'src/app/components/shared/delete-confirmation-modal/delete-confirmation-modal.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormGroup, FormControl } from '@angular/forms';
import { ApiService } from 'src/app/services/api.service';
import { Subscription } from 'rxjs';
import { Utils } from 'src/app/services/utils';
import { BaseComponent } from 'src/app/features/shared/base/base.component';
import { animate, state, style, transition, trigger } from '@angular/animations';
@Component({
  selector: 'app-stockinstore-detail',
  templateUrl: './stockinstore-detail.component.html',
  styleUrls: ['./stockinstore-detail.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class StockinstoreDetailComponent extends BaseComponent implements OnInit {

  @Output() public detailsClosed: EventEmitter<any> = new EventEmitter<any>();
  @Output() public updateTableData: EventEmitter<any> = new EventEmitter<any>();
  @Input() public stock: Stockinstore | any;
  @Input() public isDetailVisible: boolean;

  private dataService: DataEditor;
  private formDrawer: FormDrawerService;
  private deleteDialog: MatDialog;
  private apiService: ApiService;
  itemFormGroup: FormGroup;
  itemsFormControl: FormControl;
  itemSource: any;
  itemPostSource: any;
  itemPostDetailSource: any;
  expandedElement: any | null;
  private subscriptions: Subscription[] = [];
  public displayedColumns = ['item_name', 'item_uom', 'capacity'];
  public displayeditemPostColumns = ['created_at', 'item', 'customer', 'uom', 'capacity', 'qty', 'refill', 'reorder', 'fill', 'out_of_stock'];
  public displayeditemPostDetailColumns = ['created_at', 'item', 'uom', 'expiry_date', 'qty'];
  @ViewChild('tbl1', { read: MatPaginator }) tbl1: MatPaginator;
  @ViewChild('tbl2', { read: MatPaginator }) tbl2: MatPaginator;
  @ViewChild('tbl3', { read: MatPaginator }) tbl3: MatPaginator;
  initialised: boolean = false;
  public dateFilterControl;
  public customerControl;
  public customer: any[] = [];
  public damageData = [];
  public selectedTab = 0;
  public currentDate;
  constructor(apiService: ApiService, public merService: MerchandisingService, deleteDialog: MatDialog, private cts: CommonToasterService, dataService: DataEditor, formDrawer: FormDrawerService) {
    super('stock in store');
    Object.assign(this, { apiService, merService, deleteDialog, dataService, formDrawer });
    this.itemSource, this.itemPostSource, this.itemPostDetailSource = new MatTableDataSource<any>();
  }

  ngOnInit(): void {
    let today = new Date();
    let month = '' + (today.getMonth() + 1);
    let date = '' + (today.getDate());
    if ((today.getMonth() + 1) < 10) {
      month = '0' + (today.getMonth() + 1);
    }
    if ((today.getDate()) < 10) {
      date = '0' + (today.getDate());
    }
    let newdate = today.getFullYear() + '-' + month + '-' + date;
    this.currentDate = newdate;
    this.dateFilterControl = new FormControl(newdate);
    this.customerControl = new FormControl("");
    this.itemsFormControl = new FormControl([]);
    this.itemFormGroup = new FormGroup({
      items: this.itemsFormControl
    });

    this.subscriptions.push(this.apiService.getCustomers().subscribe(
      (res) => {
        this.customer = res.data;
      }));

    if (this.stock !== null || this.stock !== undefined) {
      this.setItemValues();
      if (this.itemSource !== null || this.itemSource !== undefined) {
        this.initialised = true;
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.initialised && changes) {
      if (changes.stock.currentValue != changes.stock.previousValue) {
        let currentValue = changes.stock.currentValue;
        this.stock = currentValue;
        this.setItemValues();
      }
      this.selectedTabChange(this.selectedTab);
    }
  }

  expandList(data, index) {
    this.expandedElement = this.expandedElement === data ? null : data;
    this.itemPostDetailSource = new MatTableDataSource<any>(data.assign_inventory_post_expiry);
    //console.log(this.itemPostDetailSource, index);
    setTimeout(() => {
      this.itemPostDetailSource.paginator = this.tbl3;
    });
  }

  public setItemValues(): void {
    let itemData = [];
    if (this.stock && this.stock.assign_inventory_details.length) {
      this.stock.assign_inventory_details.forEach((item, i) => {
        itemData.push({
          item_id: item?.item_id,
          item_name: item.item?.item_name,
          item_uom_id: item?.item_uom_id,
          item_uom: item.item_uom?.name,
          capacity: item.capacity
        });
      });
      if (itemData.length) {
        this.itemsFormControl.setValue(itemData);
        this.itemSource = new MatTableDataSource<any>(this.itemsFormControl.value);
        this.itemSource.paginator = this.tbl1;
      }
    } else {
      this.itemsFormControl.setValue([]);
      this.itemSource = new MatTableDataSource<any>(this.itemsFormControl.value);
    }
  }

  selectedTabChange(index) {
    switch (index) {
      case 2:
        this.getInventoryPostList('date', this.currentDate);
        break;
      case 3:
        this.getDamageItemList('date', this.currentDate);
        break;
    }
  }

  public getInventoryPostList(filter, value) {
    if (filter == "date") {
      value = this.dateFilterControl.value;
    }
    if (value == "") return false;

    this.subscriptions.push(this.merService.getInventoryPostList(this.stock.id, filter, value).subscribe(
      (res) => {
        let inventoryData = [];
        if (res.data.length > 0) {
          inventoryData = res.data;
        }
        this.itemPostSource = new MatTableDataSource<any>(inventoryData);
        this.itemPostSource.paginator = this.tbl2;

      }))
  }

  getDamageItemList(filter, value) {
    this.subscriptions.push(
      this.merService.getStockDamageItemList(this.stock.id, filter, value).subscribe((res) => {
        this.damageData = res.data;
      })
    )
  }

  public hidePaginator(len: any): boolean {
    return len < 6 ? true : false;
  }

  public closeDetailView(): void {
    this.isDetailVisible = false;
    this.detailsClosed.emit();
    this.dataService.sendData({ type: CompDataServiceType.CLOSE_DETAIL_PAGE });
  }

  public openEditstockinstore(): void {
    this.dataService.sendData({ type: CompDataServiceType.DATA_EDIT_FORM, data: this.stock });
    this.formDrawer.setFormName('add-stockinstore');
    this.formDrawer.setFormType('Edit');
    this.formDrawer.open();
  }



  public openDeleteBox(): void {
    this.deleteDialog.open(DeleteConfirmModalComponent, {
      width: '500px',
      data: { title: `Are you sure want to delete Stock In Store ${this.stock.activity_name}` }
    }).afterClosed().subscribe(data => {
      if (data.hasConfirmed) {
        this.deleteBank();
      }
    });
  }

  public deleteBank(): void {
    let delObj = { uuid: this.stock.uuid, delete: true };
    this.merService.deleteStockinstore(this.stock.uuid).subscribe(result => {
      this.closeDetailView();
      this.updateTableData.emit(delObj);
      this.cts.showSuccess("", "Deleted Successfully")
    });
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }


}
