import { Component, OnInit, OnChanges, Input, ViewChild, SimpleChanges } from '@angular/core';
import { MerchandisingService } from '../../../merchandising.service';
import { Utils } from 'src/app/services/utils';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-demage-list',
  templateUrl: './demage-list.component.html',
  styleUrls: ['./demage-list.component.scss']
})
export class DemageListComponent implements OnInit {
  @Input() public damageData;
  @Input() public stockId;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  itemSource = new MatTableDataSource();
  private subscriptions: Subscription[] = [];
  public displayedColumns = ['created_at', 'salesman', 'customer', 'item', 'damage_item_qty', 'expire_item_qty', 'saleable_item_qty'];
  public dateFilterControl;
  constructor(private merService: MerchandisingService) {
    this.itemSource = new MatTableDataSource<any>();
  }

  ngOnInit(): void {
    let today = new Date();
    let month = '' + (today.getMonth() + 1);
    let date = '' + (today.getDate());
    if ((today.getMonth() + 1) < 10) {
      month = '0' + (today.getMonth() + 1);
    }
    if ((today.getDate()) < 10) {
      date = '0' + (today.getDate());
    }
    let newdate = today.getFullYear() + '-' + month + '-' + date;
    this.dateFilterControl = new FormControl(newdate);
    this.itemSource = new MatTableDataSource<any>(this.damageData);
    this.itemSource.paginator = this.paginator;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      if (changes.damageData) {
        let currentValue = changes.damageData.currentValue;
        this.damageData = currentValue;
        this.itemSource = new MatTableDataSource<any>(this.damageData);
        this.itemSource.paginator = this.paginator;
      }
    }
  }

  getDamageItemList(filter, value) {
    if (filter == "date") {
      value = this.dateFilterControl.value;
    }
    if (value == "") return false;
    this.subscriptions.push(
      this.merService.getStockDamageItemList(this.stockId, filter, value).subscribe((res) => {
        this.itemSource = new MatTableDataSource<any>(res.data);
        this.itemSource.paginator = this.paginator;
      })
    )
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }

  public hidePaginator(len: any): boolean {
    return len < 6 ? true : false;
  }

}
