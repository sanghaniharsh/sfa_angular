import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { forkJoin, Observable, of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { NetworkService } from 'src/app/services/network.service';
import { endpoints } from 'src/app/api-list/api-end-points';
import { ApiService } from 'src/app/services/api.service';
@Injectable({
  providedIn: 'root',
})
export class MerchandisingService {
  constructor(private apiService: ApiService, private network: NetworkService) {
    Object.assign(this, { apiService });
  }

  public getStockinStore(page = 1, pageSize = 10): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.assignInventory.list +
      `?page=${page}&page_size=${pageSize}`
    );
  }

  public deleteStockinstore(uuid: string): Observable<any> {
    return this.network.onDelete(
      endpoints.apiendpoint.assignInventory.delete(uuid)
    );
  }

  public editStockinstore(uuid: string, body: any): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.assignInventory.edit(uuid),
      body
    );
  }

  public addStockinstore(body: any): Observable<any> {
    return this.network.post(endpoints.apiendpoint.assignInventory.add, body);
  }

  public getStockLists(): Observable<any> {
    const items = this.apiService.getAllItems().pipe(map((result) => result));
    const uoms = this.apiService.getAllItemUoms().pipe(map((result) => result));
    const customers = this.apiService
      .getCustomers()
      .pipe(map((result) => result));
    return forkJoin({ items, uoms, customers });
  }

  public getComplaintFeedbackListData(): Observable<any> {
    const items = this.apiService.getAllItems().pipe(map((result) => result));
    const merchandiser = this.apiService
      .getAllMerchandisers()
      .pipe(map((result) => result));
    return forkJoin({ items, merchandiser });
  }

  public getInventoryPostList(stockid, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.assignInventory.inventoryPostlist +
      `/${stockid}?${filter}=${value}`
    );
  }

  public getcomplaintsList(page = 1, pageSize = 10): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.complaintFeedback.list +
      `?page=${page}&page_size=${pageSize}`
    );
  }

  public deleteComplaintFeedback(uuid: string): Observable<any> {
    return this.network.onDelete(
      endpoints.apiendpoint.complaintFeedback.delete(uuid)
    );
  }

  public editComplaintFeedback(uuid: string, body: any): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.complaintFeedback.edit(uuid),
      body
    );
  }

  public addComplaintFeedback(body: any): Observable<any> {
    return this.network.post(endpoints.apiendpoint.complaintFeedback.add, body);
  }
  getComplaintMappingfield() {
    return this.network.getAll(
      endpoints.apiendpoint.complaintFeedback.mappingFields
    );
  }
  getPlanogramMappingfield() {
    return this.network.getAll(endpoints.apiendpoint.planogram.mappingFields);
  }
  getCompetitorInfoappingfield() {
    return this.network.getAll(
      endpoints.apiendpoint.competitor.getmappingfield
    );
  }

  public getcompetitorsList(page = 1, pageSize = 10): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.competitor.list +
      `?page=${page}&page_size=${pageSize}`
    );
  }

  public addcompetitor(body): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.competitor.add, body
    );
  }

  public editcompetitor(id, body): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.competitor.edit(id), body
    );
  }

  public getComptetitorFormLists(): Observable<any> {
    let obj = {
      "list_data": ["merchandiser", "brand"],
      "function_for": "customer"
    }
    return this.network.post(
      endpoints.apiendpoint.masterDataCollection.list, obj
    );
  }

  public importCompetitor(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.competitor.importcompetitor('import'),
      payload
    );
  }

  public ExportCompetitor(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.competitor.exportcompetitor(),
      payload
    );
  }

  public getCampaignList(page = 1, pageSize = 10): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.campaign.list +
      `?page=${page}&page_size=${pageSize}`
    );
  }

  public importCampaign(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.campaign.import('import'),
      payload
    );
  }
  public importComplaintsFeedback(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.complaintFeedback.importcomplaintFeedback('import'),
      payload
    );
  }

  public ExportCampaign(payload): Observable<any> {
    return this.network.post(endpoints.apiendpoint.campaign.export(), payload);
  }

  public importComplaints(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.complaintFeedback.importcomplaintFeedback('import'),
      payload
    );
  }
  submitImportComplaint(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.complaintFeedback.finalimport(),
      payload
    );
  }
  submitImportCompetitor(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.competitor.finalimport,
      payload
    );
  }
  submitImportAssetTracking(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.AssetTrack.finalimport,
      payload
    );
  }
  submitImportPlanogram(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.planogram.finalimport,
      payload
    );
  }
  submitFinalImportShelfDisplay(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.shelfDisplay.finalimport,
      payload
    );
  }
  submitFinalImportStockInStore(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.assignInventory.finalimport(),
      payload
    );
  }
  public exportComplaints(payload): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.complaintFeedback.exportcomplaintFeedback(),
      payload
    );
  }

  public getShelfDisplaysList(page = 1, pageSize = 10): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.shelfDisplay.list +
      `?page=${page}&page_size=${pageSize}`
    );
  }
  getShelfMappingfield() {
    return this.network.getAll(
      endpoints.apiendpoint.shelfDisplay.getmappingfield
    );
  }
  getAssetTrackingMappingfield() {
    return this.network.getAll(
      endpoints.apiendpoint.shelfDisplay.getmappingfield
    );
  }
  getStockInStoreMappingfield() {
    return this.network.getAll(
      endpoints.apiendpoint.assignInventory.mapingField()
    );
  }
  public deleteShelfDisplay(uuid: string): Observable<any> {
    return this.network.onDelete(
      endpoints.apiendpoint.shelfDisplay.delete(uuid)
    );
  }

  public editshelfDisplay(uuid: string, body: any): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.shelfDisplay.edit(uuid),
      body
    );
  }

  public addshelfDisplay(body: any): Observable<any> {
    return this.network.post(endpoints.apiendpoint.shelfDisplay.add, body);
  }

  public getModelStockList(customer_id, distribution_id): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.shelfDisplay.modelStock.customer +
      `/${customer_id}/${distribution_id}`
    );
  }

  public addModelStock(body: any): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.shelfDisplay.modelStock.add,
      body
    );
  }

  public getStockItemList(id, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.shelfDisplay.stockItemList.list +
      `/${id}?${filter}=${value}`
    );
  }

  public getDistributionImageList(id, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.shelfDisplay.distributionImageList.list +
      `/${id}?${filter}=${value}`
    );
  }

  public getDamageItemList(id, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.shelfDisplay.damageItemList.list +
      `/${id}?${filter}=${value}`
    );
  }

  public getStockDamageItemList(id, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.assignInventory.damageItemList +
      `/${id}?${filter}=${value}`
    );
  }

  public getExpiryItemList(id, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.shelfDisplay.expiryItemList.list +
      `/${id}?${filter}=${value}`
    );
  }

  public getSosList(id, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.shelfDisplay.sosList.list +
      `/${id}?${filter}=${value}`
    );
  }

  public getPlanogramList(page = 1, pageSize = 10): Observable<any> {
    const allPlanograms = this.network.getAll(
      endpoints.apiendpoint.planogram.list +
      `?page=${page}&page_size=${pageSize}`
    );
    const customers = this.apiService
      .getCustomers()
      .pipe(map((result) => result));
    return forkJoin({ allPlanograms, customers });
  }

  public editPlanogram(uuid: string, body: any): Observable<any> {
    return this.network.post(endpoints.apiendpoint.planogram.edit(uuid), body);
  }

  public deletePlanogram(uuid: string): Observable<any> {
    return this.network.onDelete(endpoints.apiendpoint.planogram.delete(uuid));
  }

  public getDistributionsList(customer_ids): Observable<any> {
    return this.apiService
      .getDistributionsBycustomers({ customer_ids: customer_ids })
      .pipe(map((result) => result));
  }

  public addPlanogram(body) {
    return this.network.post(endpoints.apiendpoint.planogram.add, body);
  }

  public getPlanogramPostList(id, filter, value) {
    return this.network.getAll(
      endpoints.apiendpoint.planogram.postList(id) + `?${filter}=${value}`
    );
  }

  public exportPlanogram(payload) {
    return this.network.post(endpoints.apiendpoint.planogram.export(), payload);
  }

  public importPlanogram(payload) {
    return this.network.post(
      endpoints.apiendpoint.planogram.import('import'),
      payload
    );
  }

  public importShelfDisplay(payload) {
    return this.network.post(
      endpoints.apiendpoint.shelfDisplay.import('import'),
      payload
    );
  }
  public importAssetTracking(payload) {
    return this.network.post(
      endpoints.apiendpoint.AssetTrack.import('import'),
      payload
    );
  }

  public importStockInStore(payload) {
    return this.network.post(
      endpoints.apiendpoint.assignInventory.import('import'),
      payload
    );
  }
  public getAssetTrackList(page = 1, pageSize = 10): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.AssetTrack.list +
      `?page=${page}&page_size=${pageSize}`
    );
  }

  public addAssetTrack(body) {
    return this.network.post(endpoints.apiendpoint.AssetTrack.add, body);
  }

  public editAssetTrack(uuid: string, body: any): Observable<any> {
    return this.network.post(endpoints.apiendpoint.AssetTrack.edit(uuid), body);
  }

  public deleteAssetTrack(uuid: string): Observable<any> {
    return this.network.onDelete(endpoints.apiendpoint.AssetTrack.delete(uuid));
  }

  public getAssetTrackPostList(id, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.AssetTrack.postList.list(id) + `?${filter}=${value}`
    );
  }

  public exportAsset(payload) {
    return this.network.post(
      endpoints.apiendpoint.AssetTrack.export(),
      payload
    );
  }

  public importAsset(payload) {
    return this.network.post(
      endpoints.apiendpoint.AssetTrack.import('import'),
      payload
    );
  }

  public getShelfDisplaySurveyList(id, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.shelfDisplay.survey.list +
      `/${id}?${filter}=${value}`
    );
  }

  public exportShelf(payload) {
    return this.network.post(
      endpoints.apiendpoint.shelfDisplay.export(),
      payload
    );
  }

  public importShelf(payload) {
    return this.network.post(
      endpoints.apiendpoint.shelfDisplay.import('import'),
      payload
    );
  }

  public getConsumerSurveyList(page = 1, pageSize = 10): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.ConsumerSurvey.list +
      `?page=${page}&page_size=${pageSize}`,
      { survey_type_id: 2 }
    );
  }

  public deleteConsumerSurvey(uuid: string): Observable<any> {
    return this.network.onDelete(
      endpoints.apiendpoint.ConsumerSurvey.delete(uuid)
    );
  }

  public getSensorySurveyList(page = 1, pageSize = 10): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.SensorySurvey.list +
      `?page=${page}&page_size=${pageSize}`,
      { survey_type_id: 3 }
    );
  }

  public deleteSensorySurvey(uuid: string): Observable<any> {
    return this.network.onDelete(
      endpoints.apiendpoint.SensorySurvey.delete(uuid)
    );
  }

  public getAssetTrackSurveyList(id, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.AssetTrack.survey.list + `/${id}?${filter}=${value}`
    );
  }

  public addSurvey(body): Observable<any> {
    return this.network.post(endpoints.apiendpoint.survey.add, body);
  }

  public addSurveyQA(body): Observable<any> {
    return this.network.post(endpoints.apiendpoint.survey.sQAPost, body);
  }

  public editSurvey(body: any, uuid: string): Observable<any> {
    return this.network.post(endpoints.apiendpoint.survey.edit(uuid), body);
  }

  public getSurveyQuestionList(id) {
    return this.network.getAll(endpoints.apiendpoint.survey.sQAList + `/${id}`);
  }

  public getSurveyQuestionAnswerList(id) {
    return this.network.getAll(
      endpoints.apiendpoint.survey.sPostQAList + `/${id}`
    );
  }

  public deleteSurveyQuestion(uuid: string): Observable<any> {
    return this.network.onDelete(endpoints.apiendpoint.survey.delete(uuid));
  }

  public getSurveyPostList(id, filter, value): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.survey.surveyPostList + `/${id}?${filter}=${value}`
    );
  }

  public getPromotionalList(page = 1, pageSize = 10): Observable<any> {
    return this.network.getAll(
      endpoints.apiendpoint.Promotional.list +
      `?page=${page}&page_size=${pageSize}`
    );
  }

  public addPromotion(body) {
    return this.network.post(endpoints.apiendpoint.Promotional.add, body);
  }

  public editPromotion(uuid: string, body: any): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.Promotional.edit(uuid),
      body
    );
  }

  public deletePromotional(uuid: string): Observable<any> {
    return this.network.onDelete(
      endpoints.apiendpoint.Promotional.delete(uuid)
    );
  }

  public getPromotionalItems(): Observable<any> {
    return this.network.getAll(endpoints.apiendpoint.Promotional.itemsList);
  }

  public getPromotionalPostList(id, filter, value) {
    return this.network.getAll(
      endpoints.apiendpoint.Promotional.postList(id) +
      `/${id}?${filter}=${value}`
    );
  }

  public downloadFile(fileurl) {
    let filename = fileurl.split('/')[fileurl.split('/').length - 1];
    let ext = filename.split('.')[filename.split('.').length - 1];
    const link = document.createElement('a');
    link.setAttribute('target', '_blank');
    link.setAttribute('href', fileurl);
    link.setAttribute('download', filename);
    ////console.log(link);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  public getReportData(body): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.merchandisingReports.ReportData,
      body
    );
  }

  public getSoaMainList(page, page_size, body): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.SOS.soaList, body
    );
  }

  public getSodMainList(page, page_size, body): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.SOS.sodList, body
    );
  }

  public getSosMainList(page, page_size, body): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.SOS.sosList, body
    );
  }

  public getFiltersDataSOS(): Observable<any> {
    const obj = {
      "list_data": ["merchandiser", "customer", "brand", "item_major_category", "item"],
      "function_for": "customer"
    }
    return this.network.post(endpoints.apiendpoint.masterDataCollection.list, obj);
  }

  public getPriceCheckList(body): Observable<any> {
    return this.network.post(
      endpoints.apiendpoint.priceCheck.list, body
    );
  }

  public getMarketPromotionList(page = 1, page_size = 10): Observable<any> {
    return this.network.getAll(endpoints.apiendpoint.MarketPromotion.list + `?page=${page}&page_size=${page_size}`);
  }
}
