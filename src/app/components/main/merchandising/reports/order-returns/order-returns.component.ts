import { ApiService } from 'src/app/services/api.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MerchandisingService } from '../../merchandising.service';
import { Utils } from 'src/app/services/utils';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';
import { DataEditor } from 'src/app/services/data-editor.service';
import { CompDataServiceType } from 'src/app/services/constants';
@Component({
  selector: 'app-order-returns',
  templateUrl: './order-returns.component.html',
  styleUrls: ['./order-returns.component.scss'],
})
export class OrderReturnsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  itemSource = new MatTableDataSource();
  private subscriptions: Subscription[] = [];
  public displayedColumns = [
    'customer_name',
    'is_return',
    'order_number',
    'order_date',
    'due_date',
    'total_discount_amount',
    'total_excise',
    'total_gross',
    'total_net',
    'total_vat',
    'grand_total',
    'created_at',
  ];
  dateFilterControl: FormControl;
  constructor(
    private apiService: ApiService,
    private merService: MerchandisingService,
    public dataEditor: DataEditor
  ) {
    this.itemSource = new MatTableDataSource<any>();
  }

  data = [];

  ngOnInit(): void {
    this.subscriptions.push(
      this.dataEditor.newData.subscribe((value) => {
        if (value.type === CompDataServiceType.REPORT_DATA) {
          //console.log(value);
          this.data = value.data;
          this.updateTableData(value.data);
        }
      })
    );
  }
  numberFormatWithSymbol(number) {
    return this.apiService.numberFormatWithSymbol(number);
  }
  updateTableData(data = []) {
    let newData = data.length > 0 ? data : this.data;
    this.itemSource = new MatTableDataSource<any>(newData);
    this.itemSource.paginator = this.paginator;
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }
}
