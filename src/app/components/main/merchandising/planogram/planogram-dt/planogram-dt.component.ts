import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  ViewChild,
  SimpleChanges,
} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { ColumnConfig } from '../../../../../interfaces/interfaces';
import { DataEditor } from '../../../../../services/data-editor.service';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { MerchandisingService } from '../../merchandising.service';
import { EventBusService } from '../../../../../services/event-bus.service';
import { FormDrawerService } from '../../../../../services/form-drawer.service';
import { MatDialog } from '@angular/material/dialog';
import { CompDataServiceType } from 'src/app/services/constants';
import { EmitEvent, Events } from '../../../../../models/events.model';
import { Utils } from '../../../../../services/utils';
import { Planogram } from '../planogram-interface';
import { PAGE_SIZE_10 } from 'src/app/app.constant';

@Component({
  selector: 'app-planogram-dt',
  templateUrl: './planogram-dt.component.html',
  styleUrls: ['./planogram-dt.component.scss'],
})
export class PlanogramDtComponent implements OnInit {
  @Output() public itemClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() public selectedRows: EventEmitter<any> = new EventEmitter<any>();
  @Input() public isDetailVisible: boolean;
  @Input() public newPlanogramData: any = {};

  public dataSource: MatTableDataSource<any>;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public selections = new SelectionModel(true, []);
  public displayedColumns: ColumnConfig[] = [];
  public filterColumns: ColumnConfig[] = [];
  private dataEditor: DataEditor;
  private subscriptions: Subscription[] = [];
  advanceSearchRequest: any[] = [];
  public apiResponse = {
    pagination: {
      total_records: 0,
    },
  };
  page = 1;
  pageSize = PAGE_SIZE_10;
  private allColumns: ColumnConfig[] = [
    { def: 'select', title: 'Select', show: true },
    { def: 'name', title: 'Name', show: true },
    { def: 'start_date', title: 'Valid From', show: true },
    { def: 'end_date', title: 'Valid To', show: true },
    // { def: 'customer', title: 'Customer', show: true },
  ];
  private collapsedColumns: ColumnConfig[] = [
    { def: 'select', title: 'Select', show: true },
    { def: 'name', title: 'Name', show: true },
  ];
  dateFilterControl: FormControl;
  allResData = [];
  constructor(
    public merService: MerchandisingService,
    dataEditor: DataEditor,
    private eventService: EventBusService,
    fds: FormDrawerService,
    deleteDialog: MatDialog
  ) {
    Object.assign(this, { merService, dataEditor, fds, deleteDialog });
    this.dataSource = new MatTableDataSource<any>();
  }

  public ngOnInit(): void {
    this.displayedColumns = this.allColumns;
    this.filterColumns = [...this.allColumns].splice(1);

    this.getPlanogramList();

    this.subscriptions.push(
      this.dataEditor.newData.subscribe((value) => {
        if (value.type === CompDataServiceType.CLOSE_DETAIL_PAGE) {
          this.closeDetailView();
        }
      })
    );
    this.subscriptions.push(
      this.eventService.on(Events.SEARCH_PLANOGRAM, ({ request, response }) => {
        this.advanceSearchRequest = [];
        if (request) {
          Object.keys(request).forEach(item => {
            this.advanceSearchRequest.push({ param: item, value: request[item] })
          })
        }

        this.updateDataSource(response);
      })
    );
  }

  onCloseCriteria() {
    this.advanceSearchRequest = []
    this.eventService.emit(new EmitEvent(Events.CHANGE_CRITERIA, { reset: true, module: Events.SEARCH_PLANOGRAM, route: '/merchandising/planograms' }));
  }
  onChangeCriteria() {
    this.eventService.emit(new EmitEvent(Events.CHANGE_CRITERIA, { route: '/merchandising/planograms' }));
  }


  getPlanogramList() {
    this.subscriptions.push(
      this.merService
        .getPlanogramList(this.page, this.pageSize)
        .subscribe((res) => {
          this.allResData = res.allPlanograms.data;
          this.apiResponse = res.allPlanograms;
          this.updateDataSource(res.allPlanograms.data);
        })
    );
  }

  onPageFired(data) {
    this.page = data['pageIndex'] + 1;
    this.pageSize = data['pageSize'];
    this.getPlanogramList();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      if (
        changes.newPlanogramData !== undefined &&
        Object.keys(changes.newPlanogramData.currentValue).length > 0
      ) {
        let currentValue = changes.newPlanogramData.currentValue;
        this.newPlanogramData = currentValue;
        this.updateAllData(this.newPlanogramData);
      }
    }
  }

  public getSelectedRows() {
    this.selectedRows.emit(this.selections.selected);
  }

  updateAllData(data) {
    this.subscriptions.push(
      this.merService
        .getPlanogramList(this.page, this.pageSize)
        .subscribe((res) => {
          this.allResData = res.allPlanograms.data;
          this.apiResponse = res.allPlanograms;
          this.updateDataSource(res.allPlanograms.data);
          this.selections = new SelectionModel(true, []);
          let tableData = res.data;
          if (data.delete !== undefined && data.delete == true) {
            this.closeDetailView();
          } else {
            if (data.edit !== undefined && data.edit == true) {
              let dataObj = tableData.filter(
                (rec) => rec.uuid.indexOf(data.uuid) !== -1
              )[0];
              this.openDetailView(dataObj);
            }
          }
        })
    );
    return false;
    let tableData = this.allResData;
    if (data.delete !== undefined && data.delete == true) {
      let indexp = tableData.filter(
        (rec) => rec.uuid.indexOf(data.uuid) !== -1
      )[0];
      let index = tableData.indexOf(indexp);
      tableData.splice(index, 1);
      this.closeDetailView();
    } else {
      if (data.edit !== undefined && data.edit == true) {
        let indexp = tableData.filter(
          (rec) => rec.uuid.indexOf(data.uuid) !== -1
        )[0];
        let index = tableData.indexOf(indexp);
        tableData[index] = data;
        this.openDetailView(data);
      } else {
        tableData.push(data);
      }
    }
    this.allResData = tableData;
    this.updateDataSource(tableData);
  }

  updateDataSource(data) {
    //console.log(data);
    this.dataSource = new MatTableDataSource<any>(data);
    // this.dataSource.paginator = this.paginator;
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }

  public openDetailView(data: Planogram): void {
    this.isDetailVisible = true;
    this.itemClicked.emit(data);
    this.updateCollapsedColumns();
  }

  public closeDetailView(): void {
    this.isDetailVisible = false;
    this.updateCollapsedColumns();
  }

  private updateCollapsedColumns(): void {
    this.displayedColumns = this.isDetailVisible
      ? this.collapsedColumns
      : this.allColumns;
  }

  public getDisplayedColumns(): string[] {
    return this.displayedColumns
      .filter((column) => column.show)
      .map((column) => column.def);
  }

  public isAllSelected(): boolean {
    return this.selections.selected.length === this.dataSource.data.length;
  }

  public toggleSelection(): void {
    this.isAllSelected()
      ? this.selections.clear()
      : this.dataSource.data.forEach((row) => this.selections.select(row));
  }

  public checkboxLabel(row?: Planogram): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selections.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1
      }`;
  }

  getPaginatorValue(len: number) {
    return len < 10 ? true : false;
  }
}
