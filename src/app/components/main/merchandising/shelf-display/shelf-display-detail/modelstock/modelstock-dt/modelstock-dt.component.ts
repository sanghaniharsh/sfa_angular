import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MerchandisingService } from '../../../../merchandising.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-modelstock-dt',
  templateUrl: './modelstock-dt.component.html',
  styleUrls: ['./modelstock-dt.component.scss']
})
export class ModelstockDtComponent implements OnInit {
  @Input() public itemsData: any[];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  itemSource = new MatTableDataSource();
  public displayedColumns = ['item_code', 'item', 'item_uom', 'capacity', 'total_number_of_facing'];
  constructor(private merService: MerchandisingService) {
    this.itemSource = new MatTableDataSource<any>();
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.itemSource = new MatTableDataSource(this.itemsData['distribution_model_stock_details']);
    this.itemSource.paginator = this.paginator;
  }

  public hidePaginator(len: any): boolean {
    return len < 6 ? true : false;
  }

}
