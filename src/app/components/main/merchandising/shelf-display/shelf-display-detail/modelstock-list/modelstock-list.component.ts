import { Component, OnInit, OnChanges, Input, ViewChild, SimpleChanges } from '@angular/core';
import { MerchandisingService } from '../../../merchandising.service';
import { Utils } from 'src/app/services/utils';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ShelfDisplay } from '../../shelf-display-interface';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-modelstock-list',
  templateUrl: './modelstock-list.component.html',
  styleUrls: ['./modelstock-list.component.scss']
})
export class ModelstockListComponent implements OnInit {
  @Input() public stockData;
  @Input() public distribution_id;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  itemSource = new MatTableDataSource();
  private subscriptions: Subscription[] = [];
  public displayedColumns = ['created_at', 'salesman', 'customer', 'item', 'capacity', 'saleable', 'is_out_of_stock'];
  public dateFilterControl;
  constructor(private merService: MerchandisingService) {
    this.itemSource = new MatTableDataSource<any>();
  }
  ngOnInit(): void {
    let today = new Date();
    let month = '' + (today.getMonth() + 1);
    let date = '' + (today.getDate());
    if ((today.getMonth() + 1) < 10) {
      month = '0' + (today.getMonth() + 1);
    }
    if ((today.getDate()) < 10) {
      date = '0' + (today.getDate());
    }
    let newdate = today.getFullYear() + '-' + month + '-' + date;
    this.dateFilterControl = new FormControl(newdate);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      if (changes.stockData) {
        let currentValue = changes.stockData.currentValue;
        this.stockData = currentValue;
        this.itemSource = new MatTableDataSource<any>(this.stockData);
        this.itemSource.paginator = this.paginator;
      }
    }
  }

  getStockItemList(filter, value) {
    if (filter == "date") {
      value = this.dateFilterControl.value;
    }
    if (value == "") return false;
    this.subscriptions.push(
      this.merService.getStockItemList(this.distribution_id, filter, value).subscribe((res) => {
        this.itemSource = new MatTableDataSource<ShelfDisplay>(res.data);
        this.itemSource.paginator = this.paginator;
      })
    )
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }

  public hidePaginator(len: any): boolean {
    return len < 6 ? true : false;
  }

}
