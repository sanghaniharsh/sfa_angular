import { Component, OnInit, OnChanges, Input, ViewChild, SimpleChanges } from '@angular/core';
import { MerchandisingService } from '../../../merchandising.service';
import { Utils } from 'src/app/services/utils';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-share-of-shelf',
  templateUrl: './share-of-shelf.component.html',
  styleUrls: ['./share-of-shelf.component.scss']
})
export class ShareOfShelfComponent implements OnInit {

  @Input() public sosData;
  @Input() public distribution_id;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  itemSource = new MatTableDataSource();
  private subscriptions: Subscription[] = [];
  public displayedColumns = ['created_at', 'salesman', 'customer', 'item', 'total_facing', 'actual_facing', 'score'];
  dateFilterControl: FormControl;
  constructor(private merService: MerchandisingService) {
    this.itemSource = new MatTableDataSource<any>();
  }

  ngOnInit(): void {
    let today = new Date();
    let month = '' + (today.getMonth() + 1);
    let date = '' + (today.getDate());
    if ((today.getMonth() + 1) < 10) {
      month = '0' + (today.getMonth() + 1);
    }
    if ((today.getDate()) < 10) {
      date = '0' + (today.getDate());
    }
    let newdate = today.getFullYear() + '-' + month + '-' + date;
    this.dateFilterControl = new FormControl(newdate);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes) {
      if (changes.sosData) {
        let currentValue = changes.sosData.currentValue;
        this.sosData = currentValue;
        this.itemSource = new MatTableDataSource<any>(this.sosData);
        this.itemSource.paginator = this.paginator;
      }
    }
  }

  getSosList(filter, value) {
    if (filter == "date") {
      value = this.dateFilterControl.value;
    }
    if (value == "") return false;
    this.subscriptions.push(
      this.merService.getSosList(this.distribution_id, filter, value).subscribe((res) => {
        this.itemSource = new MatTableDataSource<any>(res.data);
        this.itemSource.paginator = this.paginator;
      })
    )
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }

  public hidePaginator(len: any): boolean {
    return len < 6 ? true : false;
  }

}
