import { Component, OnInit, OnChanges, Input, ViewChild, SimpleChanges } from '@angular/core';
import { MerchandisingService } from '../../../merchandising.service';
import { Utils } from 'src/app/services/utils';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-expiry-list',
  templateUrl: './expiry-list.component.html',
  styleUrls: ['./expiry-list.component.scss']
})
export class ExpiryListComponent implements OnInit {

  @Input() public expiryData;
  @Input() public distribution_id;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  itemSource = new MatTableDataSource();
  private subscriptions: Subscription[] = [];
  public displayedColumns = ['created_at', 'salesman', 'customer', 'item', 'qty', 'distribution_name', 'expiry_date'];
  dateFilterControl: FormControl;
  constructor(private merService : MerchandisingService) { 
    this.itemSource = new MatTableDataSource<any>();
  }

  ngOnInit(): void {
    let today = new Date();
    let month = ''+(today.getMonth()+1);
    let date = ''+(today.getDate());
    if((today.getMonth()+1) < 10){
      month = '0'+(today.getMonth()+1);
    }
    if((today.getDate()) < 10){
      date = '0'+(today.getDate());
    }
    let newdate = today.getFullYear()+'-'+month+'-'+date;
    this.dateFilterControl = new FormControl(newdate);
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes) {
      if(changes.expiryData) {
        let currentValue = changes.expiryData.currentValue;
        this.expiryData = currentValue;
        this.itemSource = new MatTableDataSource<any>(this.expiryData);
        this.itemSource.paginator = this.paginator;
      }
    }
  }

  getExpiryItemList(filter, value){
    if(filter == "date"){
      value = this.dateFilterControl.value;
    }
    if(value == "") return false;
    this.subscriptions.push(
      this.merService.getExpiryItemList(this.distribution_id, filter, value).subscribe((res) => {
        this.itemSource = new MatTableDataSource<any>(res.data);
        this.itemSource.paginator = this.paginator;
      })
    )
  }

  public ngOnDestroy(): void {
    Utils.unsubscribeAll(this.subscriptions);
  }

  public hidePaginator(len: any): boolean {
    return len < 6 ? true : false;
  }

}
