import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
})
export class DashboardPageComponent implements OnInit {
  dashboardIndex: 0;
  navLinks: any[];
  activeLinkIndex = 1;
  domain = window.location.host.split('.')[0];
  constructor(private router: Router) { }
  ngOnInit(): void {
    this.navLinks = [
      {
        label: 'Dashboard 1',
        link: './board1',
        index: 0
      },
      {
        label: 'Dashboard 2',
        link: './board2',
        index: 1
      },
      {
        label: 'Dashboard 3',
        link: './board3',
        index: 2
      },
      {
        label: 'Dashboard 4',
        link: './board4',
        index: 3
      },
    ];

    if (this.domain !== 'nfpc') {
      this.navLinks.push({
        label: 'Dashboard 5',
        link: './board5',
        index: 4
      })
    }
    this.router.events.subscribe((res) => {
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
    });
  }
  tabChanged(item) {
    this.dashboardIndex = item.index;
  }
}
