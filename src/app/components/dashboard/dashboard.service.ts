import { ApiService } from './../../services/api.service';
import { MasterService } from './../main/master/master.service';
import { Observable, forkJoin } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map, filter } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private http: HttpClient, public masterService: MasterService, public apiService: ApiService) { }

  getData(): Observable<any> {
    const headers = new HttpHeaders().set(
      'Content-Type',
      'application/x-www-form-urlencoded'
    );
    return this.http
      .get<any>('../../assets/constants/dashboard.json', { headers })
      .pipe(map((r) => r));
  }

  getDashboardFiltersData(filterdata): Observable<any> {
    const obj = {
      "list_data": ["region", "channel", "merchandiser", "salesman_supervisor"],
      "function_for": "customer"
    }

    const masterData = this.masterService.masterList(obj).pipe(map(result => result));
    const dashboardData = this.apiService.getDashboarddata(filterdata).pipe(map(result => result));
    return forkJoin({ masterData, dashboardData });
  }

  getDashboardDataByFilter(body): Observable<any> {
    return this.apiService.getDashboarddata(body).pipe(map(result => result));
  }
}
