import { HttpClientModule } from '@angular/common/http';
import { MaterialImportModule } from './../../imports/material-import/material-import.module';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/features/shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardPageComponent } from './views/dashboard-page/dashboard-page.component';
import { CalendarModule } from 'angular-calendar';
import { MerchandisingDashboardComponent } from './components/merchandising-dashboard/merchandising-dashboard.component';
import { CommonDashboardComponent } from './components/common-dashboard/common-dashboard.component';
import { MerchandisingDetailComponent } from './components/merchandising-detail/merchandising-detail.component';
import { DashboardChartDetailComponent } from './components/dashboard-chart-detail/dashboard-chart-detail.component';
import { MerchandisingDashboard2Component } from './components/merchandising-dashboard2/merchandising-dashboard2.component';
import { MerchanidisingDashboard3Component } from './components/merchanidising-dashboard3/merchanidising-dashboard3.component';
import { MerchandisingDashboard4Component } from './components/merchandising-dashboard4/merchandising-dashboard4.component';

@NgModule({
  declarations: [
    DashboardPageComponent,
    MerchandisingDashboardComponent,
    CommonDashboardComponent,
    MerchandisingDetailComponent,
    DashboardChartDetailComponent,
    MerchandisingDashboard2Component,
    MerchanidisingDashboard3Component,
    MerchandisingDashboard4Component,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialImportModule,
    DashboardRoutingModule,
    CalendarModule,
    HttpClientModule,
  ],
})
export class DashboardModule { }
