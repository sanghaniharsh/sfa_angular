import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonDashboardComponent } from './components/common-dashboard/common-dashboard.component';
import { MerchandisingDashboardComponent } from './components/merchandising-dashboard/merchandising-dashboard.component';
import { MerchandisingDashboard2Component } from './components/merchandising-dashboard2/merchandising-dashboard2.component';
import { MerchandisingDashboard4Component } from './components/merchandising-dashboard4/merchandising-dashboard4.component';
import { MerchanidisingDashboard3Component } from './components/merchanidising-dashboard3/merchanidising-dashboard3.component';
import { DashboardPageComponent } from './views/dashboard-page/dashboard-page.component';

const routes: Routes = [
  {
    path: '', component: DashboardPageComponent,
    children: [
      { path: 'board1', component: MerchandisingDashboardComponent },
      { path: 'board2', component: MerchandisingDashboard2Component },
      { path: 'board3', component: MerchanidisingDashboard3Component },
      { path: 'board4', component: MerchandisingDashboard4Component },
      { path: 'board5', component: CommonDashboardComponent },
    ]
  },

];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule { }
