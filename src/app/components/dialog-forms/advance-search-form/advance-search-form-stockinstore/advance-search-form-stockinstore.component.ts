import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { STATUS } from 'src/app/app.constant';

@Component({
  selector: 'app-advance-search-form-stockinstore',
  templateUrl: './advance-search-form-stockinstore.component.html',
  styleUrls: ['./advance-search-form-stockinstore.component.scss']
})
export class AdvanceSearchFormStockinstoreComponent implements OnInit {
  statusList:Array<any>=STATUS;
  @Input() salesman:Array<any>=[]
  form:FormGroup
  constructor() { }

  ngOnInit(): void {
    this.form=new FormGroup({
      module:new FormControl('stockinstore'),
      startdate: new FormControl(),
      enddate: new FormControl(),
      salesman: new FormControl(),
    })
  }

}
