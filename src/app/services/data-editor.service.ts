import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataEditor {

  private dataSource: BehaviorSubject<any> = new BehaviorSubject<any>({});

  public newData: Observable<any> = this.dataSource.asObservable();

  public nextSubjectData = new Subject<any>();
  public nextSubjectDataObj = this.nextSubjectData.asObservable();

  constructor() { }

  public sendData(newData: any): void {
    this.dataSource.next(newData);
  }

  public nextSourceData(data: any): void {
    this.nextSubjectData.next(data);
  }
}
